<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InvoiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [InvoiceController::class, 'index'])->name('invoice.index');
Route::get('create-invoice', [InvoiceController::class, 'createInvoice'])->name('create-invoice');
Route::get('products', [InvoiceController::class, 'products'])->name('products');
Route::post('submit-product', [InvoiceController::class, 'submitProduct'])->name('submit-product');
Route::get('brands', [InvoiceController::class, 'brand'])->name('brands');
Route::get('show-brands', [InvoiceController::class, 'showBrand'])->name('show-brand');
Route::get('show-products', [InvoiceController::class, 'showProducts'])->name('show-products');
Route::post('submit-brand', [InvoiceController::class, 'submitBrand'])->name('submit-brand');
Route::get('edit-brand/{id}', [InvoiceController::class, 'editBrand'])->name('edit-brand');
Route::get('edit-product/{id}', [InvoiceController::class, 'editProduct'])->name('edit-product');
Route::post('update-brand/{id}', [InvoiceController::class, 'updateBrand'])->name('update-brand');
Route::post('update-product/{id}', [InvoiceController::class, 'updateProduct'])->name('update-product');
Route::post('get-product', [InvoiceController::class, 'getProduct'])->name('get-product');
Route::post('submit-details', [InvoiceController::class, 'submitDetails'])->name('submit-details');
Route::post('submit-charge', [InvoiceController::class, 'submitCharge'])->name('submit-charge');
Route::get('delete-product/{id}', [InvoiceController::class, 'deleteProduct'])->name('delete-product');
Route::post('update-invoice', [InvoiceController::class, 'updateInvoice'])->name('update-invoice');
Route::post('get-office-charge', [InvoiceController::class, 'getOfficeCharge'])->name('get-office-charge');
Route::post('generate-pdf', [InvoiceController::class, 'generatePDF'])->name('generate-pdf');
Route::get('invoice-list', [InvoiceController::class, 'invoiceList'])->name('invoice-list');
Route::get('filter-invoice', [InvoiceController::class, 'filterInvoice'])->name('filter-invoice');
Route::get('edit-invoice/{id}', [InvoiceController::class, 'editInvoice'])->name('edit-invoice');
Route::post('update-details/{id}', [InvoiceController::class, 'updateDetails'])->name('update-details');
Route::post('update-pdf/{id}', [InvoiceController::class, 'updatePDF'])->name('update-pdf');
Route::post('filter-data', [InvoiceController::class, 'filterData'])->name('filter-data');
Route::get('view-product/{id}', [InvoiceController::class, 'viewProduct'])->name('view-product');
