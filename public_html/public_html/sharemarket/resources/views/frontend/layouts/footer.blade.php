
 <footer>
        <div class="uk-container uk-light">
            <div class="uk-grid uk-margin-large-top">
                <div class="uk-width-1-4@l uk-width-1-5@m uk-width-1-3@s">
                    <p>
 <a class="uk-logo" href="{{asset('/')}}"><img src="{{asset('public/images/logo.png')}}" alt="fina finance" /></a>

                    </p>
                    <p>
                    तुम्ही demat account काढले, पुढे काय?
शेअर मार्केट मध्ये गुंतवणूक कुठे आणि कशी करावी?
या विषयावर मराठी भाषेतून मार्गदर्शन खास तुमच्यासाठी..
आजच संपर्क करा </p>
                        <p>  
                        आमचे विषय:
                            </p>
                     <ul class="uk-list">
<li><a href="#">१. शेअर मार्केट</a></li>
<li><a href="#">२. Money Management</a></li>
<li><a href="#">३. Business Growth</a></li>
                         
                    </ul>
                   
                </div>
                <div class="uk-width-1-4@l uk-width-1-5@m uk-width-1-3@s">
                    <h5>Quick Links</h5>
                    <ul class="uk-list">

                        <li><a href="{{asset('/')}}">Home</a></li>
                        <li><a href="{{asset('/about')}}">About</a></li>
                        <li><a href="{{asset('/courses')}}">Courses</a></li>
                        <li><a href="{{asset('/blog')}}">Blog</a></li>
                        <li><a href="{{asset('/faq')}}">Faq</a></li>
                        <li><a href="{{asset('/contact')}}">Contact Us</a></li>
                    </ul>
                </div>
                <div class="uk-width-1-4@l uk-width-1-5@m uk-width-1-3@s">
                    <ul class="uk-list">
                        <h5>Contact Us</h5>

                        <li><a href="#">
                          <i class="fa fa-map-marker" aria-hidden="true"></i> Pune, Maharashtra, India.</a></li>
                        <li><a href="tel:+91-9022596464"><i class="fa fa-phone" aria-hidden="true"></i> +91-9022596464</a></li>
                        <li><a href="mailto:info@kalimmshaikhsschool.com"><i class="fa fa-envelope" aria-hidden="true"></i>
info@kalimmshaikhsschool.com</a></li>
                    </ul>
                </div>
                <div class="uk-width-1-4@l uk-width-2-5@m uk-width-1-1@s">
                    <div class="uk-align-right idz-footer-adjust">
                        <a href="#" class="uk-icon-button uk-margin-small-right" data-uk-icon="icon: facebook"></a>
                        <a href="#" class="uk-icon-button  uk-margin-small-right" data-uk-icon="icon: twitter"></a>
                        <a href="#" class="uk-icon-button  uk-margin-small-right" data-uk-icon="icon: instagram"></a>
                        <a href="#" class="uk-icon-button" data-uk-icon="icon: youtube"></a>
                    </div>
                    
                </div>
                <div class="uk-width-1-1 uk-margin-large-top uk-margin-large-bottom">
                    <!-- logo footer begin -->
                    <div id="footer-logo">
                        <p><span>Copyright ©2023 Kalimm Shaikh's School. All Rights Reserved.</span></p>
                    </div>
                    <!-- logo footer end -->
                    <hr>
                    <div class="uk-margin-small-top">
                        <p>येणाऱ्या १० ते १५ वर्षात पैसा आपल्यासाठी काम करेल तो कसा ह्या system शिकण्यासाठी आजच Action/ Admission घ्या.
आता जबाबदारी घेण्याची वेळ आली आहे. आता स्वतः ट्रेड करण्याची वेळ आली आहे.</p>
                    </div>
                </div>
            </div>
        </div>
        <a class="uk-inline" href="#" data-uk-totop data-uk-scroll data-uk-scrollspy="cls: uk-animation-fade; hidden: true; offset-top: 100px; repeat: true"></a>
    </footer>
    

    <!-- Javascript -->
    <script src="{{asset('public/js/jquery.js')}}"></script>
    <script src="{{asset('public/js/uikit.min.js')}}"></script>
    <script src="{{asset('public/js/uikit-icons.min.js')}}"></script>
    <script src="{{asset('public/js/components/mediaelement.js')}}"></script>    
    <script src="{{asset('public/js/config.js')}}"></script>
<script type="text/javascript" src="https://www.rkglobal.net/js/wow.min.js"></script>
<script type="text/javascript" src="https://www.rkglobal.net/js/jquery.marquee.min.js"></script>
<script type="text/javascript" src="https://www.rkglobal.net/js/rkglobalscript.js?ver=1.23"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>

<div class="fixed-whatsapp">
    
         <a href="https://api.whatsapp.com/send?phone=+91-9022596464%20&text=Enquiry%20from%20website%20https://classicfire.co.in/sharemarket/" target="_blank"><span class="fa fa-whatsapp" ></span></a>
    
    </div>
    
    <div class="fixed-phone">
        <a href="tel:+91-9022596464"> <i class="fa fa-phone"></i></a></div>
        
       <script>
$(document).ready(function(){
        $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    dots:false,
    nav:true,
    autoplay:true,   
    smartSpeed: 3000, 
    autoplayTimeout:7000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
});

});
</script> 
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
  <script>
$(document).ready(function(){
   setTimeout(function(){
       $('#myModal').modal('show');
   }, 500);
});
</script>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("selectshow");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.selectdropbtn')) {
    var selectdropdowns = document.getElementsByClassName("selectdropdown-content");
    var i;
    for (i = 0; i < selectdropdowns.length; i++) {
      var openDropdown = selectdropdowns[i];
      if (openDropdown.classList.contains('selectshow')) {
        openDropdown.classList.remove('selectshow');
      }
    }
  }
}
</script>
<div class="fixed-whatsapp">
    
         <a href="https://api.whatsapp.com/send?phone=+91-9022596464%20&text=Enquiry%20from%20website%20https://classicfire.co.in/sharemarket/" target="_blank"><span class="fa fa-whatsapp" ></span></a>
    
    </div>
    
    <div class="fixed-phone">
        <a href="tel:+91-9022596464"> <i class="fa fa-phone"></i></a></div>
</body>
</html>