<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Site Properties -->
   <title>	Kalimm Shaikh's School</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('public/css/uikit.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/style.css')}}">
    <link rel="stylesheet" href="https://www.indonez.com/error/404.html">
    <link rel="stylesheet" href="{{asset('public/css/components/font-awesome.css')}}"> </head>
<link rel="stylesheet" href="{{asset('public/css/eocnewsticker.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
           <link rel="stylesheet" href="  https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
          <!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">-->
<body>
    <header>
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-4@s">
                    <!-- logo header begin -->
                    <div id="header-logo">
                        <a class="uk-logo" href="{{asset('/')}}"><img src="{{asset('public/images/logo.png')}}" alt="fina finance" /></a>
                    </div>
                    <!-- logo header end -->
                </div>
                <div class="uk-width-3-5@l uk-width-2-3@m uk-width-3-4@s uk-visible@m">
                    <div class="uk-grid top-right">
                        
                        <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-4@s top-rightlft">
                              <a class="uk-button uk-button-primary" href="#">Free DEMAT</a> 
                                 <a class="uk-button uk-button-secondary" href="#">Free Courses</a>
                                 </div>

                        <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-4@s top-rightcenter">
                              <span class="select-label">Languages:</span>
                            <div class="idz-mini-nav uk-align-right">
                               
                                <form class="uk-display-inline">
                                    <div class="selectdropdown">
  <button onclick="myFunction()" type="button" class="selectdropbtn">                    <span>
                                            <span class="idz-globe"><i class="fa fa-globe"></i></span>
                                            <span></span>
                                        </span>Language</button>
  <div id="myDropdown" class="selectdropdown-content">
    <a href="#">English</a>
    <a href="#">Hindi</a>
    <a href="#">Marathi</a>
  </div>
</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    <!--<div data-uk-form-custom="target: > * > span:last-child">-->
                                    <!--    <select>-->
                                    <!--        <option value="1"><a href="https://www.w3schools.com">Visit W3Schools.com!</a></option>-->
                                    <!--        <option value="1">Hindi</option>-->
                                    <!--        <option value="2">Marathi</option>-->
                                            
                                    <!--    </select>-->
                    
                                    <!--</div>-->
                                </form>
                            </div>
                        </div>
                        <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-4@s phone-number top-rightrht">
                            <h2 class="uk-align-right">+91-9022596464</h2>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    
            <!--         <hr> -->
                    <!-- Main navigation begin -->
                    <nav class="uk-navbar-container uk-visible@m" data-uk-navbar style="z-index: 980;" data-uk-sticky="animation: uk-animation-slide-top; cls-active: uk-sticky; cls-inactive: uk-navbar-container; bottom: #offset">
                        <div class="uk-navbar-left">
                            <ul class="uk-navbar-nav">
                                               <li class="uk-active"> <a href="{{asset('/')}}">Home</a>
                                </li>
                                <li><a href="{{asset('/about')}}">ABOUT</a></li>
                              
                                          <li> <a href="#">COURSES</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav idz-dropdown-nopadding">
                                            <li><a href="{{asset('/courses-details')}}">Aspirant Foundation </a></li>
                                             <li><a href="{{asset('/courses-details')}}">Technical analysis</a></li>
                                                                                      <li><a href="{{asset('/courses-details')}} ">Fundamental Analysis</a></li>
                                             <li><a href="{{asset('/courses-details')}}">Master Course ( Future& Option  ]</a></li> 
                                                                                       
                                             <li><a href="{{asset('/courses-details')}}">Mutual Fund</a></li>
                                             <li><a href="{{asset('/courses-details')}}">Govt. Bond</a></li>
                                                                                       <li><a href="{{asset('/courses-details')}} ">Prime Portfolio</a></li>
                                             <li><a href="{{asset('/courses-details')}}">Small Portfolio</a></li>
                                            
                                        </ul>
                                    </div>
                                </li>
                                          
                                                                       <li> <a href="#">Investment</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav idz-dropdown-nopadding">
                                            <li><a href="{{asset('/investment-details')}}">Mutual Fund </a></li>
                                             <li><a href="{{asset('/investment-details')}}">SIP</a></li>
 <li><a href="{{asset('/investment-details')}}">Insurance</a></li>
                                              <li><a href="{{asset('/investment-details')}}">Government Bond</a></li>
 <li><a href="{{asset('/investment-details')}}">Prime Portfolio</a></li>
                                            
                                        </ul>
                                    </div>
                                </li>

        <li> <a href="#">TESTIMONIAL</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav idz-dropdown-nopadding">
                                            <li><a href="{{asset('/Photo-testimonial')}}">Photos Testimonial </a></li>
                                            <li><a href="{{asset('/video-testimonial')}}">Videos Testimonial </a></li>
                                        </ul>
                                    </div>
                                </li>

<li><a href="{{asset('/event')}}">Event</a></li>
        <li> <a href="#">GALLERY</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav idz-dropdown-nopadding">
                                            <li><a href="{{asset('/gallery-photos')}}">Photos</a></li>
                                            <li><a href="{{asset('/gallery-videos')}}">Videos</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li><a href="{{asset('/contact')}}">CONTACT US</a></li>
                            </ul>
                        </div>
                        <div class="uk-navbar-right uk-visible@l">
                            <div data-uk-margin>
                            <a class="uk-button uk-button-default" href="#">Free Webinar</a> 
                             <a class="uk-button uk-button-secondary" href="#">DOWNLOAD BROCHURE</a> 
                            
                            
                            </div>
                            
                        </div>
                    </nav>
                    <!-- Main navigation end -->
                    <!-- Mobile navigation begin -->
                    <a class="uk-button uk-align-center idz-mobile-nav uk-hidden@m" href="#modal-full" data-uk-icon="icon: menu" data-uk-toggle>Menu</a>
                    <div id="modal-full" class="uk-modal-full" data-uk-modal>
                        <div class="uk-modal-dialog">
                            <button class="uk-modal-close-full uk-close-large" type="button" data-uk-close></button>
                            <div data-uk-height-viewport>
                                <div class="uk-position-cover uk-overlay uk-overlay-primary uk-flex uk-flex-center uk-flex-middle">
                                    <ul class="uk-nav-primary uk-nav-parent-icon" data-uk-nav>
                                <li class="uk-active"> <a href="index.html">Home</a>
                                </li>
                                <li><a href="about.html">ABOUT</a></li>
                                <li><a href="courses.html">COURSES</a></li>
                        
                                <li><a href="#">GALLERY</a></li>
<li><a href="news.html">BLOG</a></li>
<li><a href="testimonials.html">TESTIMONIAL</a></li>
                                <li><a href="contact.html">CONTACT US</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Mobile navigation end -->
                </div>
            </div>
        </div>
    </header>