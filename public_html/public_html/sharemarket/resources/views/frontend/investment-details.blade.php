@extends('frontend.layouts.main')

@section('main-container')
                <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>Investment Title</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Investment Title</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-2-3@l uk-width-2-3@m uk-width-1-1@s">
                    <article class="uk-article idz-article">
                       <h3 class="uk-margin-small-top">Investment Title Goes Here..</h3> <img class="uk-margin-bottom" src="{{asset('public/images/content/blog_picture.jpg')}}" alt="">
                        <div class="">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        
                        
                        </div>
                    </article>
                </div>
                <div class="uk-width-1-3@l uk-width-1-3@m uk-width-1-1@s">


                    <aside class="uk-margin-medium-bottom">
                        <div class="uk-card uk-card-default idz-widget-card uk-card-body">
                            <h5 class="uk-text-uppercase uk-margin-remove-bottom">Other Courses</h5>
                            <ul class="uk-list uk-list-divider idz-categories-widget">
<li><a href="#">Mutual Fund<span class="uk-float-right" data-uk-icon="icon: triangle-right; ratio: 0.9"></span></a></li>
<li><a href="#">SIP<span class="uk-float-right" data-uk-icon="icon: triangle-right; ratio: 0.9"></span></a></li>
  <li><a href="#">Insurance<span class="uk-float-right" data-uk-icon="icon: triangle-right; ratio: 0.9"></span></a></li>
  <li><a href="#">Government Bond<span class="uk-float-right" data-uk-icon="icon: triangle-right; ratio: 0.9"></span></a></li>
    <li><a href="#">Prime Portfolio<span class="uk-float-right" data-uk-icon="icon: triangle-right; ratio: 0.9"></span></a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
   @endsection