@extends('frontend.layouts.main')

@section('main-container')
    <section id="slideshow-container">
        <!-- Slideshow wrapper begin -->
        <div data-uk-slideshow="autoplay: true; animation: slide; max-height: 370">
            <div class="uk-position-relative">
                <ul class="uk-slideshow-items">
                    <!-- Slide 1 begin -->
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center">
                            <img src="{{asset('public/images/slideshow/bg_slide1.jpg')}}" alt="" uk-cover>
                        </div>
                        <div class="uk-container uk-position-center">
                            <div class="uk-grid">
                                <div class="uk-width-3-4@l uk-width-1-1@s uk-margin-medium-left">
               
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Slide 1 end -->
                    <!-- Slide 2 begin -->
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center">
                            <img src="{{asset('public/images/slideshow/bg_slide1.jpg')}}" alt="" uk-cover>
                        </div>
                        <div class="uk-container uk-position-center">
                            <div class="uk-grid">
                                <div class="uk-width-1-1 uk-align-center uk-text-center">
                            

                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Slide 2 end -->
                    <!-- Slide 3 begin -->
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center">
                            <img src="{{asset('public/images/slideshow/bg_slide1.jpg')}}" alt="" uk-cover>
                        </div>
                        <div class="uk-container uk-position-center">
                            <div class="uk-grid">
                                <div class="uk-width-3-4@l uk-width-1-1@s uk-margin-medium-left">
              
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Slide 3 end -->
                </ul>
            </div>
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>
        <!-- Slideshow wrapper end -->
    </section>    
    
    

    <!--<section class="uk-margin-medium-bottom">-->
    <!--    <div class="uk-container uk-container-expand uk-padding-remove idz-stock-ticker-container" data-uk-slider="autoplay: true,finite: true;">-->
    <!--</div>-->
    <!--</section>-->
      <div class="footmarque">
    <div class="footmarqudiv">
        <div class='footmarqudata'>
             <div class="idz-stock-ticker-price">
                        Alphabet Inc (GOOGL)                    
                        <span class="idz-stock-ticker-down">1,134.42<span class="ticker-down"></span>(1.41%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Microsoft Corp (MSFT)                    
                        <span class="idz-stock-ticker-up">94.60<span class="ticker-up"></span>(0.45%)</span>    
                    </div>    
               
              
                    <div class="idz-stock-ticker-price">
                        General Motors (GM)                    
                        <span class="idz-stock-ticker-up">37.94<span class="ticker-up"></span>(0.24%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Apple Inc (AAPL)                    
                        <span class="idz-stock-ticker-down">178.02<span class="ticker-down"></span>(0.35%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Tesla Inc (TSLA)                    
                        <span class="idz-stock-ticker-down">321.35<span class="ticker-up"></span>(1.31%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Alibaba Group (BABA)                    
                        <span class="idz-stock-ticker-up">200.28<span class="ticker-up"></span>(0.59%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Amazon.com (AMZN)                    
                        <span class="idz-stock-ticker-down">1,571.68<span class="ticker-down"></span>(0.67%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Exxon Mobil (XOM)                    
                        <span class="idz-stock-ticker-up">75.12<span class="ticker-up"></span>(0.94%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Walmart Inc (WMT)                    
                        <span class="idz-stock-ticker-up">89.17<span class="ticker-up"></span>(1.90%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Chevron Corp (CVX)                    
                        <span class="idz-stock-ticker-down">115.40<span class="ticker-down"></span>(0.16%)</span>    
                    </div>
               
            
          </div>
    </div>
</div>

    <section>
        
        
        <div class="uk-container" style="margin-bottom: 50px;">
            <div class="uk-grid">
                  
                <div class="uk-width-3-5@l uk-width-2-3@m uk-width-1-1@s">
                    <div class="uk-label uk-label-success uk-margin-small-bottom uk-margin-top">ABOUT US</div>
                    <h2 class="uk-margin-small-top uk-margin-remove-bottom">Take Quick and Right Decision at
Right Time for Better Profit</h2>
                    <p class="uk-text-lead uk-margin-small-bottom">We understand that education is the most powerful weapon to help bring the desired change in our society and to contribute to the society at large. So we were established with the thought of providing best quality education to the students in order to let them achieve their dreams.</p>
                    <p class="uk-text-lead uk-text-success uk-margin-top"><span class="uk-text-bold">Learn</span>&nbsp; ~ &nbsp;<span class="uk-text-bold">Understand</span>&nbsp; ~ &nbsp;<span class="uk-text-bold">Trade</span></p>


                    <a class="uk-button uk-button-primary" href="#">Learn More</a>
                </div>
                <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-1@s">
                    <div class="uk-card uk-card-medium uk-card-secondary uk-card-body uk-margin-top">
                        <h6 class="uk-text-uppercase uk-margin-remove-bottom">Kalimm Shaikh's School</h6><br>
                       <iframe width="330" height="200" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>
                </div>
            </div>
            </div>
    
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div class="uk-card uk-card-default uk-card-body">
                      
                        <h2 class="uk-text-center">Most Popular Courses to Study in
Kalimm Shaikhs School
</h2>
                        <div class="uk-child-width-1-4@l uk-child-width-1-2@m uk-child-width-1-1@s" data-uk-grid>
                            <div class="uk-text-center"> <img class="uk-margin-bottom" src="{{asset('public/images/video-1.jpg')}}" alt="">
                                <h6 class="uk-text-uppercase uk-text-muted uk-margin-remove-top uk-margin-small-bottom">Basic Knowledge</h6>
                                <p class="uk-margin-remove-top">Also included here explanation of Stock Market Terms and jargon used by people involved in trading stocks.</p>
                            </div>
                            <div class="uk-text-center"> <img class="uk-margin-bottom" src="{{asset('public/images/video-2.jpg')}}" alt="">
                                <h6 class="uk-text-uppercase uk-text-muted uk-margin-remove-top uk-margin-small-bottom">Technical Analysis</h6>
                                <p class="uk-margin-remove-top">Technical analysis helps guide traders to what is most likely to happen given past information.</p>
                            </div>
                            <div class="uk-text-center"> <img class="uk-margin-bottom" src="{{asset('public/images/video-3.jpg')}}" alt="">
                                <h6 class="uk-text-uppercase uk-text-muted uk-margin-remove-top uk-margin-small-bottom">Master Course</h6>
                                <p class="uk-margin-remove-top">Learn how to trade stocks and improve your stock market investments.</p>
                            </div>
                            <div class="uk-text-center"> <img class="uk-margin-bottom" src="{{asset('public/images/video-4.jpg')}}" alt="">
                                <h6 class="uk-text-uppercase uk-text-muted uk-margin-remove-top uk-margin-small-bottom">Candle Stick Pattern</h6>
                                <p class="uk-margin-remove-top">The candlestick charts are used for identifying trading patterns which help the technical analyst to set up their trades.</p>
                            </div>
                        </div>
                        
                        <div class="uk-text-center uk-margin-small-bottom"> <a class="uk-button uk-button-primary" href="courses.html">View All Cources</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


       <section class="uk-margin-large-bottom idz-invest-product">
        <div class="uk-container">
            <div class="uk-grid uk-margin-small-top">
                <div class="uk-width-3-5@l uk-width-1-1@m uk-width-1-1@s">
                    <h2 class="uk-margin-remove-bottom">A relationship on your terms.</h2>
                    <h3 class="uk-margin-remove-top idz-thin">Work with us the way you want.</h3>
                    <p class="uk-text-lead">Some believe you must choose between an online broker and a wealth management firm. At Fina, you don’t need to compromise. Whether you invest on your own, with an advisor, or a little of both — we can support you.</p>
                    <div class="uk-child-width-1-3@l uk-child-width-1-3@m uk-child-width-1-2@s uk-grid-small uk-grid-match uk-margin-medium-top" data-uk-grid>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card green">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Investing</h6>
                                <p class="uk-margin-remove-top">A wide selection of investment product to help build diversified portfolio</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card blue">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Trading</h6>
                                <p class="uk-margin-remove-top">Powerful trading tools, resources and support</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card purple">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Wealth Management</h6>
                                <p class="uk-margin-remove-top">Dedicated financial consultant to help reach your own specific goals</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card midnight">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Investment advisory</h6>
                                <p class="uk-margin-remove-top">A wide selection of investing strategies from seasoned portfolio managers</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card grey">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Smart portfolio</h6>
                                <p class="uk-margin-remove-top">A revolutionary, fully-automated investmend advisory services</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card orange">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Advisor network</h6>
                                <p class="uk-margin-remove-top">Specialized guidance from independent local advisor for hight-net-worth investors</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="uk-margin-medium-bottom">
        <div class="uk-container uk-container-expand uk-background-fixed uk-background-cover uk-background-center-center" style="background-image: url({{asset('public/images/content/content_background.jfif')}});">
            <div class="uk-container uk-margin-medium-top">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h2 class="uk-text-center uk-margin-medium-bottom">Take on the market with our powerful platforms.</h2>
                        <div class="uk-child-width-1-2" data-uk-grid>
                            <div class="uk-width-1-3@l uk-width-2-5@m">
                                <ul class="uk-list uk-margin-medium-bottom idz-platform-icon">
                                    <li><i class="fa fa-3x fa-windows"></i>Windows</li>
                                    <li><i class="fa fa-3x fa-apple"></i>OS X &amp; iOS</li>
                                    <li><i class="fa fa-3x fa-android"></i>Android</li>
                                    <li><i class="fa fa-3x fa-internet-explorer"></i>Browser</li>
                                </ul>
                                <div class="uk-card uk-card-default uk-card-medium uk-card-body idz-card-custom">
                                    <h6 class="uk-text-uppercase uk-text-success">FITS - Fina Trading System</h6>
                                    <p class="uk-text-lead">Our flagship trading platform, designed for all kinds of traders</p>
                                    <p>Trade from your desktop or on-the-go with the most powerful &amp; convenient trading platform for any platform.</p>
                                    <div class="uk-text-center uk-margin-small-bottom"> <a class="uk-button uk-button-primary" href="#">Download FITS 2.0</a> </div>
                                </div>
                            </div>
                            <div class="uk-width-2-3@l uk-width-3-5@m uk-inline"> <img class="uk-align-center uk-position-bottom idz-media-adjust1" src="{{asset('public/images/content/content_sample1.png')}}" alt=""> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="uk-text-center uk-margin-medium-bottom">Knowledge empowers you</h2>
                    <div class="uk-child-width-1-3@l uk-child-width-1-2@s uk-grid-medium uk-grid-match" data-uk-grid>
                        <div>
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-book"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Beginners Course</h6>
                                        <p class="uk-margin-remove-top">Learn the basic concepts of trading, what this market is all about, and why you should be a part of it.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-gears"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Trading Tools</h6>
                                        <p class="uk-margin-remove-top">Familiarize yourself with advanced strategies and FITS trading toolset. Take your trading to the next level.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-area-chart"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Technical Analysis</h6>
                                        <p class="uk-margin-remove-top">Technical analysis uses chart patterns and techniques to predict future price movements.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-calculator"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Fundamental Analysis</h6>
                                        <p class="uk-margin-remove-top">Fundamental analysts consider all available data to help them to determine the relative value of a market.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-support"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Risk Management</h6>
                                        <p class="uk-margin-remove-top">Get to know why you should focus on position sizing and why risk management is crucial to trading success.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body idz-resources-card red">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-youtube-play"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-margin-small-bottom">Video Tutorial</h6>
                                        <p class="uk-margin-remove-top">Various trading principle and study case video tutorials from our professional trainers and practitioners.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
   @endsection
