@extends('frontend.layouts.main')

@section('main-container')
                <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>Videos Gallery</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Videos Gallery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

   <!-- about-area-start -->
   <div class="tp-about-area pt-60 pb-120 uk-margin-medium-top">
      <div class="container">
             <div class="row">
             
             <div class="col-lg-12">
                 <h2 class="uk-text-center">Our  Videos Gallery
</h2>
                 
                 </div>
                 
                 </div>
         <div class="row">
             
             <div class="col-lg-12">
             
   <!-- tp-slider-area end -->
   <div class="tp-portfolio-area pt-20 pb-10 uk-margin-medium-top">
<section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div class="uk-child-width-1-3@l uk-child-width-1-2@s uk-grid-medium uk-grid-match uk-grid" data-uk-grid="">
                        <div class="uk-first-column grid-item">
<a href="{{asset('https://www.youtube.com/embed/MC6wBExX-N4')}}" data-fancybox="gallery" data-caption="Caption #1">
<iframe width="560" style="width:100% !important;" height="315" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</a>
                        </div>
                        <div>

            <div class="tp-port-item mb-30 grid-item">
<a href="{{asset('https://www.youtube.com/embed/MC6wBExX-N4')}}" data-fancybox="gallery" data-caption="Caption #2">
<iframe width="560" style="width:100% !important;" height="315" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

</a>
               </div>

                        </div>
                       
                        <div class="uk-grid-margin uk-first-column grid-item ">
<a href="{{asset('https://www.youtube.com/embed/MC6wBExX-N4')}}" data-fancybox="gallery" data-caption="Caption #3">
<iframe width="560" style="width:100% !important;" height="315" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

</a>

                        </div>
                        <div class="uk-grid-margin grid-item ">
<a href="{{asset('public/images/content/popular_thumb1.jpg')}}" data-fancybox="gallery" data-caption="Caption #4">
<iframe width="560" style="width:100% !important;" height="315" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

</a>
                        </div>
                             <div class="uk-grid-margin grid-item ">
<a href="{{asset('https://www.youtube.com/embed/MC6wBExX-N4')}}" data-fancybox="gallery" data-caption="Caption #5">
<iframe width="560" style="width:100% !important;" height="315" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

</a>
                        </div>
                             <div class="uk-grid-margin grid-item ">
<a href="{{asset('public/images/content/popular_thumb3.jpg')}}" data-fancybox="gallery" data-caption="Caption #6">
<iframe width="560" style="width:100% !important;" height="315" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

</a>
                        </div>
 
                    </div>
                </div>
            </div>
        </div>
    </section>
      
   </div>
             
             </div>
             
             
         </div>
          
      </div>
   </div>
   <!-- service-area-start -->
   @endsection