@extends('frontend.layouts.main')

@section('main-container')

                  <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>Video Testimonials</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Video Testimonials</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-1-1">
                    <h2 class="uk-margin-remove-bottom uk-text-center">What Our Clients Say ?</h2>
                    <p class="uk-margin-small-top uk-text-lead uk-text-center">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur</p>
                    <div class="uk-margin-medium-top uk-margin-medium-bottom">
                        <hr> </div>
                    <div class="uk-child-width-1-2@m uk-grid-match" data-uk-grid>
                      <div>
                            <div class="idz-testimonials">
                                  
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team1.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                </div>
                                               </div>
                        </div>
                      <div>
                            <div class="idz-testimonials">
                                  
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team2.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                </div>
                                               </div>
                        </div>
                      <div>
                            <div class="idz-testimonials">
                                  
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team3.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                </div>
                                               </div>
                        </div>
                      <div>
                            <div class="idz-testimonials">
                                  
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team1.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                </div>
                                               </div>
                        </div>
                      <div>
                            <div class="idz-testimonials">
                                  
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team2.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                </div>
                                               </div>
                        </div>
                      <div>
                            <div class="idz-testimonials">
                                  
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team3.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                </div>
                                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   @endsection