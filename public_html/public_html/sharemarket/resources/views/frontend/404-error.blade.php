@extends('frontend.layouts.main')

@section('main-container')
    <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>404 Error</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">404 Error</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-3-5@l uk-width-3-5@m uk-width-1-1@s uk-align-center uk-margin-large-top uk-margin-large-bottom">
                    <div class="uk-card uk-card-secondary uk-card-body idz-404-background">
                        <h1 class="uk-heading-primary uk-margin-remove-bottom">oops! 404</h1>
                        <p class="uk-text-lead uk-margin-small-top">The page you are looking for might have been removed
                            <br/>had its name changed or is temporarily unavailable</p>
                        <hr/>
                        <h4 class="uk-margin-remove-top">you can go to this link</h4>
                        <ul class="uk-subnav uk-subnav-divider" data-uk-margin>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Investment Product</a></li>
                            <li><a href="#">Education</a></li>
                            <li><a href="#">Market News</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
   @endsection