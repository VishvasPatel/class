@extends('frontend.layouts.main')

@section('main-container')
    <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>About Us</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

  <section class="about-section">

                   <div class="uk-container" style="margin-bottom: 50px;">
            <div class="uk-grid paddtp">
                  
                <div class="uk-width-3-5@l uk-width-2-3@m uk-width-1-1@s">
                    <div class="uk-label uk-label-success uk-margin-small-bottom">ABOUT US</div>
                    <h2 class="uk-margin-small-top uk-margin-remove-bottom">KALIMM SHAIKH'S SCHOOL
 </h2>
                      <p class="uk-text-lead uk-margin-small-bottom ">We understand that education is the most powerful 
                      weapon to help bring the desired change in our society and to contribute to the society at large.
                      So we were established with the thought of providing best quality education to the students 
                      in order to let them achieve their dreams.</p>
   
 <h4 class="uk-margin-small-top uk-margin-small-bottom">Certificates:</h4>
             <div class="dispblock">
                        <div class="">
<img class="uk-margin-bottom" src="{{asset('public/images/certificate-1.jfif')}}" alt="">
                        </div>
                        <div>
<img class="uk-margin-bottom" src="{{asset('public/images/certificate-2.jfif')}}" alt="">
                        </div>

                    </div>                  

        
                </div>
                <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-1@s">
                    
                         
                    <div class="home-map">
                                 <img  src="{{asset('public/images/map.jfif')}}" alt="">
                       
                                       </div>
                    
              <div class="uk-margin-top">
                        <h6 class="uk-text-uppercase uk-margin-remove-bottom">Kalimm Shaikh's School</h6><br>
                       <iframe width="330" height="200" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>
              

                                       
                                       
            </div>
            </div>
            

            </div>

    </section>
             <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-1-1">
                    <h2 class="uk-margin-remove-bottom uk-text-center">What Our Clients Say ?</h2>
                    <p class="uk-margin-small-top uk-text-lead uk-text-center">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur</p>
                    <div class="uk-margin-medium-top uk-margin-medium-bottom">
                        <hr> </div>
                    <div class="uk-child-width-1-2@m uk-grid-match" data-uk-grid>
                        <div>
                            <div class="idz-testimonials">
                                 <h3 class="uk-text-center">Photo Testimonial</h3>
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy 
                                        text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s.</p>
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team1.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>David Stone</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                    
                                    <a class="uk-button uk-button-primary" href="{{asset('/Photo-testimonial')}}">View All</a>
                                    
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="idz-testimonials">
                                  <h3 class="uk-text-center">Video Testimonial</h3>
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team3.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                        <a class="uk-button uk-button-secondary" href="{{asset('/video-testimonial')}}">View All</a>
                                </div>
                                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   @endsection