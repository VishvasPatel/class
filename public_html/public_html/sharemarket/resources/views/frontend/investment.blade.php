@extends('frontend.layouts.main')

@section('main-container')

                <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>Investment</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Investment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-margin-large-bottom investbg paddtpbtm investment-section margtop50">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="uk-text-center uk-margin-medium-bottom">Investment</h2>
                    <div class="uk-child-width-1-3@l uk-child-width-1-2@s uk-grid-medium uk-grid-match" data-uk-grid>
                        <div>
                            <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-book"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Mutual Fund</h6>
                                        <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-gears"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">SIP</h6>
                                                <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-area-chart"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Insurance</h6>
                                   <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-calculator"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Government Bond</h6>
                                     <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-support"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Prime Portfolio</h6>
                                   <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
             <div class="uk-text-center margtp50">    <a class="uk-button uk-button-default" href="{{asset('/investment')}}">View More</a><div>
                </div>
            </div>
        </div>
    </section>
       <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-1-1">
                    <h2 class="uk-margin-remove-bottom uk-text-center">What Our Clients Say ?</h2>
                    <p class="uk-margin-small-top uk-text-lead uk-text-center">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur</p>
                    <div class="uk-margin-medium-top uk-margin-medium-bottom">
                        <hr> </div>
                    <div class="uk-child-width-1-2@m uk-grid-match" data-uk-grid>
                        <div>
                            <div class="idz-testimonials">
                                 <h3 class="uk-text-center">Photo Testimonial</h3>
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy 
                                        text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s.</p>
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team1.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>David Stone</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                    
                                    <a class="uk-button uk-button-primary" href="{{asset('/Photo-testimonial')}}">View All</a>
                                    
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="idz-testimonials">
                                  <h3 class="uk-text-center">Video Testimonial</h3>
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team3.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                        <a class="uk-button uk-button-secondary" href="{{asset('/video-testimonial')}}">View All</a>
                                </div>
                                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   @endsection