@extends('frontend.layouts.main')

@section('main-container')
            <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>Courses</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Courses</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="uk-margin-large-bottom grey-bg paddtpbtm">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="uk-text-center uk-margin-medium-bottom">Courses</h2>
                    <div class="uk-child-width-1-3@l uk-child-width-1-2@s uk-grid-medium uk-grid-match" data-uk-grid>
                        <div class="courses_item">
                             <a href="{{asset('/courses-details')}}">
           <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card green">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Technical analysis</h6>
                                <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        </a>
                        </div>
                       <div class="courses_item">
                             <a href="{{asset('/courses-details')}}">
                  <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card blue">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Fundamental Analysis</h6>
                                                  <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        </a>
                        </div>
                          <div class="courses_item">
 <a href="{{asset('/courses-details')}}">
                        <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card purple">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Master Course ( Future& Option  ]</h6>
                                                <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        </a>
                        </div>
                          <div class="courses_item">
                             <a href="{{asset('/courses-details')}}">
              <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card orange">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Mutual Fund</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        </a>
                        </div>
                        <div class="courses_item">
                             <a href="{{asset('/courses-details')}}">
            <div>
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card midnight">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Investment advisory</h6>
                                        <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                        </div>
                        </a>
                        </div>
                             <div class="courses_item">
                                <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card grey">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Govt. Bond</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
                        </div>
                        
                                                                        <div class="courses_item">
                                                                             <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card purple">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Prime Portfolio</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
                        </div>
                                                                                                  <div class="courses_item">
                                                                                                    <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card blue">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Small Portfolio</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="single.html"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
                        </div> 
                        
                    </div>

                </div>
            </div>
        </div>
    </section>

        
        
        
   @endsection