@extends('frontend.layouts.main')

@section('main-container')

                <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>FAQ</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="uk-margin-top uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-2-3@l uk-width-2-3@m uk-width-1-1@s">
                    <ul data-uk-accordion="collapsible: false" class="uk-margin-medium-bottom">
                        <li>
                            <h3 class="uk-accordion-title">Excepteur sint occaecat ?</h3>
                            <div class="uk-accordion-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcorob laboris nisi ut aliquip praesentium voluptatum deleniti.</p>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.</p>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Officia deserunt mollit anim ?</h3>
                            <div class="uk-accordion-content">
                                <p>Consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt neque porro quisquam est, qui dolorem ipsum quia dolor sit amet omnis voluptas assumenda est</p>
                                <ul class="uk-list idz-list-check">
                                    <li>Quis autem iure reprehenderit voluptate molestiae</li>
                                    <li>Et harum quidem rerum facilis est et expedita distinctio</li>
                                    <li>Temporibus autem quibusdam et aut officiis debitis</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Inventore veritatis architecto ?</h3>
                            <div class="uk-accordion-content"> <img class="uk-align-left uk-margin-remove-adjacent uk-box-shadow-medium" src="{{asset('public/images/content/img_sample.jpg')}}" alt="">
                                <h5 class="uk-margin-remove-bottom">Image on content</h5>
                                <p class="uk-margin-small-top">At vero eos et accusamus et iusto odio dignissimos ducimus qui facilis blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui offici deserunt mollitia animi idest laborum et dolorum fuga eta harum quidem rerum.</p>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Reprehenderit in voluptate velit ?</h3>
                            <div class="uk-accordion-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcorob laboris nisi ut aliquip praesentium voluptatum deleniti.</p>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.</p>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Necessitatibus saepe eveniet ?</h3>
                            <div class="uk-accordion-content">
                                <div class="uk-child-width-1-2@s" data-uk-grid>
                                    <div>
                                        <h5 class="uk-margin-remove-bottom">2 Column</h5>
                                        <p class="uk-margin-small-top">Nam libero tempore cumsoluta lut nobis esteligel dioptiorop cumque nihilermo impedit quota minus quodrobu maxime placeatus facere ud possimusi omnishor voluptasi assumend omnisar repellend</p>
                                    </div>
                                    <div>
                                        <h5 class="uk-margin-remove-bottom">2 Column</h5>
                                        <p class="uk-margin-small-top">Nam libero tempore cumsoluta lut nobis esteligel dioptiorop cumque nihilermo impedit quota minus quodrobu maxime placeatus facere ud possimusi omnishor voluptasi assumend omnisar repellend</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Consequatur aut alias perferendis ?</h3>
                            <div class="uk-accordion-content"> <img class="uk-margin-remove-bottom uk-box-shadow-medium" src="{{asset('public/images/content/img_sample2.jpg')}}" alt="">
                                <p>Neque porro quisquam qui dolorem ipsum quia dolor sitamet consectetur adipisci velitsed quialo numquam eius modi tempora incidunt ut labore etus dolore magnam aliquam quaerat voluptatem vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate provident similique sunt in culpa qui officia deserunt mollitia animi.</p>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Numquam eius modi tempora ?</h3>
                            <div class="uk-accordion-content">
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi occaecati cupiditateno provident, similique sunt in culpa qui officia deserunt mollitia animi, idest laborum et dolorum fuga harum quidem rerum facilis est et expedita porro distinctio.</p>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Voluptatibus maiores alias ?</h3>
                            <div class="uk-accordion-content">
                                <p>Consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt neque porro quisquam est, qui dolorem ipsum quia dolor sit amet omnis voluptas assumenda est</p>
                                <ul class="uk-list idz-list-check">
                                    <li>Quis autem iure reprehenderit voluptate molestiae</li>
                                    <li>Et harum quidem rerum facilis est et expedita distinctio</li>
                                    <li>Temporibus autem quibusdam et aut officiis debitis</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Doloribus asperiores repellat ?</h3>
                            <div class="uk-accordion-content"> <img class="uk-align-right uk-margin-remove-adjacent uk-box-shadow-medium" src="{{asset('public/images/content/img_sample.jpg')}}" alt="">
                                <h5 class="uk-margin-remove-bottom">Image on content</h5>
                                <p class="uk-margin-small-top">At vero eos et accusamus et iusto odio dignissimos ducimus qui facilis blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui offici deserunt mollitia animi idest laborum et dolorum fuga eta harum quidem rerum.</p>
                            </div>
                        </li>
                        <li>
                            <h3 class="uk-accordion-title">Numquam modi tempora incidunt ?</h3>
                            <div class="uk-accordion-content">
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-1-3@l uk-width-1-3@m uk-width-1-1@s">
                    <aside class="uk-margin-medium-bottom">
                        <h5 class="uk-text-uppercase uk-margin-remove-bottom">Our Advantage</h5>
                        <ul class="uk-list uk-list-divider idz-popular-widget">
                            <li>
                                <div class="uk-grid-small" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-briefcase uk-margin-small-top"></i> </div>
                                    <div class="uk-width-expand uk-margin-small-left">
                                        <h5 class="uk-margin-remove-bottom">Robust Framework</h5>
                                        <p class="uk-margin-small-top">voluptatibus maiores consequatur aut perferendis doloribus asperiores</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="uk-grid-small" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-cube uk-margin-small-top"></i> </div>
                                    <div class="uk-width-expand uk-margin-small-left">
                                        <h5 class="uk-margin-remove-bottom">Modern &amp; Clean</h5>
                                        <p class="uk-margin-small-top">voluptatibus maiores consequatur aut perferendis doloribus asperiores</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="uk-grid-small" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-money uk-margin-small-top"></i> </div>
                                    <div class="uk-width-expand uk-margin-small-left">
                                        <h5 class="uk-margin-remove-bottom">Competitive Price</h5>
                                        <p class="uk-margin-small-top">voluptatibus maiores consequatur aut perferendis doloribus asperiores</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </aside>
                    <aside class="uk-margin-medium-bottom">
                        <div class="uk-card uk-card-primary uk-card-body">
                            <h5 class="uk-text-uppercase uk-margin-remove-bottom">Want Hire Us ?</h5>
                            <p class="uk-margin-small-top">Duis aute irure dolor reprehenderit voluptate velit esse cillum dolore eu fugiatu pariatur officia deserunt</p> <a class="uk-button uk-button-primary" href="#">Get in touch</a> </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
   @endsection