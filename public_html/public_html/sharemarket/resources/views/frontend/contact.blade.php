@extends('frontend.layouts.main')

@section('main-container')
    <section id="pagetitle-container">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2>Contact</h2>
                    <ul class="uk-breadcrumb uk-float-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-1-1 uk-margin-medium-bottom">
                    <h2>Looking for Help?</h2>
                    <p class="uk-text-lead">At Fina, we are always here to assist and guide you, doing all we can to help you succeed.
                        <br/>Our client support team is available to assist you 24 hours a day, 5 days a week.</p>
                    <div class="uk-card uk-card-default uk-card-medium uk-margin-medium-top">
                        <div class="uk-card-body">
                            <div class="uk-child-width-1-3@m uk-grid-divider uk-grid-medium uk-grid-match" data-uk-grid>
                                <div>
                                    <h6 class="uk-text-uppercase uk-margin-remove-bottom">Investor Relations</h6>
                                    <p class="uk-margin-small-top">For existing or prospective Limited Partner/investor inquiries</p>
                                    <ul class="uk-list idz-list-contact uk-text-success uk-margin-remove-top">
                                        <li><span class="uk-label uk-label-success"><i class="fa fa-envelope"></i></span> <span class="uk-margin-small-left">investorrelations@fina.com</span></li>
                                    </ul>
                                </div>
                                <div>
                                    <h6 class="uk-text-uppercase uk-margin-remove-bottom">Public Relations</h6>
                                    <p class="uk-margin-small-top">For press, speaking or marketing related inquiries</p>
                                    <ul class="uk-list idz-list-contact uk-text-success uk-margin-remove-top">
                                        <li><span class="uk-label uk-label-success"><i class="fa fa-envelope"></i></span> <span class="uk-margin-small-left">publicrelations@fina.com</span></li>
                                    </ul>
                                </div>
                                <div>
                                    <h6 class="uk-text-uppercase uk-margin-remove-bottom">Business Plan Submissions</h6>
                                    <p class="uk-margin-small-top">For business plan submissions. Please submit here or using form below.</p>
                                    <ul class="uk-list idz-list-contact uk-text-success uk-margin-remove-top">
                                        <li><span class="uk-label uk-label-success"><i class="fa fa-envelope"></i></span> <span class="uk-margin-small-left">businessplan@fina.com</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="uk-card-media-bottom">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2@l uk-width-1-2@m uk-width-1-1@s">
                    <div class="uk-child-width-1-2@s uk-grid-divider idz-content-adjust2 uk-margin-small-top uk-margin-large-bottom" data-uk-grid>
                        <div>
                            <h6 class="uk-text-uppercase uk-margin-remove-bottom">Manhattan Office</h6>
                            <p class="uk-margin-small-top uk-margin-remove-bottom">Level 34, Scott Square,
                                <br/>Scott Street, 4268, New York,
                                <br/>United States America</p> <span class="uk-label uk-label-success uk-margin-small-top uk-margin-medium-bottom">Tel : (800) 123-4455</span> </div>
                        <div>
                            <h6 class="uk-text-uppercase uk-margin-remove-bottom">London Office</h6>
                            <p class="uk-margin-small-top uk-margin-remove-bottom">Level 8, One Square,
                                <br/>Canary Wharf, E14 5AA, London
                                <br/>United Kingdom</p> <span class="uk-label uk-label-success uk-margin-small-top uk-margin-medium-bottom">Tel : (399) 123-8822</span> </div>
                        <div class="uk-width-1-1 uk-margin-remove-top uk-margin-remove-bottom">
                            <hr class="uk-visible@m"> </div>
                        <div class="uk-margin-remove-top uk-visible@m">
                            <h6 class="uk-text-uppercase uk-margin-medium-top uk-margin-remove-bottom">Singapore Office</h6>
                            <p class="uk-margin-small-top uk-margin-remove-bottom">Level 2, Cintech Building,
                                <br/>73 Science Park Dr, 118254
                                <br/>Singapore</p> <span class="uk-label uk-label-success uk-margin-small-top">Tel : (720) 123-9933</span> </div>
                        <div class="uk-margin-remove-top uk-visible@m">
                            <h6 class="uk-text-uppercase uk-margin-medium-top uk-margin-remove-bottom">Tokyo Office</h6>
                            <p class="uk-margin-small-top uk-margin-remove-bottom">Level 11, Compass Offices,
                                <br/>Toranomon, Minatoku, 105-01
                                <br/>Japan</p> <span class="uk-label uk-label-success uk-margin-small-top">Tel : (655) 123-3377</span> </div>
                    </div>
                </div>
                <div class="uk-width-1-2@l uk-width-1-2@m uk-width-1-1@s">
                    <h3>Drop Us a line</h3>
                    <form id="contact-form" class="uk-form">
                        <div class="uk-margin uk-width-2-3">
                            <input class="uk-input" id="name" value="" type="text" placeholder="Full name"> </div>
                        <div class="uk-margin uk-width-2-3">
                            <input class="uk-input" id="email" value="" type="email" placeholder="Email"> </div>
                        <div class="uk-margin uk-width-2-3">
                            <input class="uk-input" id="subject" value="" type="text" placeholder="Subject"> </div>
                        <div class="uk-margin">
                            <textarea class="uk-textarea" id="message" rows="5" placeholder="Message"></textarea>
                        </div>
                        <div>
                            <button class="uk-button uk-button-primary uk-float-left" id="buttonsend" type="submit" name="submit">Send Message</button>
                            <div class="idz-contact-loading uk-float-left uk-margin-left" style="display: none;"><span data-uk-spinner></span>Please wait..</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
   @endsection