@extends('frontend.layouts.main')

@section('main-container')
    <section id="slideshow-container">
        <!-- Slideshow wrapper begin -->
        <div data-uk-slideshow="autoplay: true; animation: slide; max-height: 370">
            <div class="uk-position-relative">
                <ul class="uk-slideshow-items">
                    <!-- Slide 1 begin -->
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center">
                            <img src="{{asset('public/images/slideshow/bg_slide1.jpg')}}" alt="" uk-cover>
                        </div>
                        <div class="uk-container uk-position-center">
                            <div class="uk-grid">
                                <div class="uk-width-3-4@l uk-width-1-1@s uk-margin-medium-left">
               
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Slide 1 end -->
                    <!-- Slide 2 begin -->
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center">
                            <img src="{{asset('public/images/slideshow/bg_slide1.jpg')}}" alt="" uk-cover>
                        </div>
                        <div class="uk-container uk-position-center">
                            <div class="uk-grid">
                                <div class="uk-width-1-1 uk-align-center uk-text-center">
                            

                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Slide 2 end -->
                    <!-- Slide 3 begin -->
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center">
                            <img src="{{asset('public/images/slideshow/bg_slide1.jpg')}}" alt="" uk-cover>
                        </div>
                        <div class="uk-container uk-position-center">
                            <div class="uk-grid">
                                <div class="uk-width-3-4@l uk-width-1-1@s uk-margin-medium-left">
              
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Slide 3 end -->
                </ul>
            </div>
            <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
        </div>
        <!-- Slideshow wrapper end -->
    </section>    
    

      <div class="footmarque">
    <div class="footmarqudiv">
        <div class='footmarqudata'>
             <div class="idz-stock-ticker-price">
                        Alphabet Inc (GOOGL)                    
                        <span class="idz-stock-ticker-down">1,134.42<span class="ticker-down"></span>(1.41%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Microsoft Corp (MSFT)                    
                        <span class="idz-stock-ticker-up">94.60<span class="ticker-up"></span>(0.45%)</span>    
                    </div>    
               
              
                    <div class="idz-stock-ticker-price">
                        General Motors (GM)                    
                        <span class="idz-stock-ticker-up">37.94<span class="ticker-up"></span>(0.24%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Apple Inc (AAPL)                    
                        <span class="idz-stock-ticker-down">178.02<span class="ticker-down"></span>(0.35%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Tesla Inc (TSLA)                    
                        <span class="idz-stock-ticker-down">321.35<span class="ticker-up"></span>(1.31%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Alibaba Group (BABA)                    
                        <span class="idz-stock-ticker-up">200.28<span class="ticker-up"></span>(0.59%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Amazon.com (AMZN)                    
                        <span class="idz-stock-ticker-down">1,571.68<span class="ticker-down"></span>(0.67%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Exxon Mobil (XOM)                    
                        <span class="idz-stock-ticker-up">75.12<span class="ticker-up"></span>(0.94%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Walmart Inc (WMT)                    
                        <span class="idz-stock-ticker-up">89.17<span class="ticker-up"></span>(1.90%)</span>    
                    </div>
               
              
                    <div class="idz-stock-ticker-price">
                        Chevron Corp (CVX)                    
                        <span class="idz-stock-ticker-down">115.40<span class="ticker-down"></span>(0.16%)</span>    
                    </div>
               
            
          </div>
    </div>
</div>

    <section class="about-section">

                   <div class="uk-container" style="margin-bottom: 50px;">
            <div class="uk-grid paddtp">
                  
                <div class="uk-width-3-5@l uk-width-2-3@m uk-width-1-1@s">
                    <div class="uk-label uk-label-success uk-margin-small-bottom">ABOUT US</div>
                    <h2 class="uk-margin-small-top uk-margin-remove-bottom">KALIMM SHAIKH'S SCHOOL
 </h2>
                      <p class="uk-text-lead uk-margin-small-bottom ">We understand that education is the most powerful weapon to help bring the desired change in our society and to contribute to the society at large. So we were established with the thought of providing best quality education to the students in order to let them achieve their dreams.</p>
   
 <h4 class="uk-margin-small-top uk-margin-small-bottom">Certificates:</h4>
             <div class="dispblock">
                        <div class="">
<img class="uk-margin-bottom" src="{{asset('public/images/certificate-1.jfif')}}" alt="">
                        </div>
                        <div>
<img class="uk-margin-bottom" src="{{asset('public/images/certificate-2.jfif')}}" alt="">
                        </div>

                    </div>                  

                    <a class="uk-button uk-button-primary" href="{{asset('/about')}}">Learn More</a>
                </div>
                <div class="uk-width-2-5@l uk-width-1-3@m uk-width-1-1@s">
                    
                         
                    <div class="home-map">
                                 <img  src="{{asset('public/images/map.jfif')}}" alt="">
                       
                                       </div>
                    
              <div class="uk-margin-top">
                        <h6 class="uk-text-uppercase uk-margin-remove-bottom">Kalimm Shaikh's School</h6><br>
                       <iframe width="330" height="200" src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>
              

                                       
                                       
            </div>
            </div>
            

            </div>

    </section>

    <section class="uk-margin-large-bottom grey-bg paddtpbtm">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="uk-text-center uk-margin-medium-bottom">Courses</h2>
                    
                         <div class="uk-child-width-1-3@l uk-child-width-1-2@s 
           uk-grid-medium uk-grid-match" data-uk-grid>
                             <div class="owl-carousel owl-theme">
<div class="courses_item">

  <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card green">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Technical analysis</h6>
                                <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>

</div>                        
<div class="courses_item">
    <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card blue">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Fundamental Analysis</h6>
                                                  <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>                        
<div class="courses_item">
        <a href="{{asset('/courses-details')}}">
                     <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card purple">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Master Course ( Future& Option  ]</h6>
                                                <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>
<div class="courses_item">
    <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card orange">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Mutual Fund</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>                        
<div class="courses_item">
    <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card midnight">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Investment advisory</h6>
                                        <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>                        
<div class="courses_item">
    <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card grey">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Govt. Bond</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>
<div class="courses_item">
        <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card purple">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Prime Portfolio</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>                        
<div class="courses_item">
        <a href="{{asset('/courses-details')}}">
                            <div class="uk-card uk-card-primary uk-card-small uk-card-body uk-inline idz-invest-card blue">
                                <h6 class="uk-text-uppercase uk-margin-small-bottom">Small Portfolio</h6>
                                                       <p class="uk-margin-remove-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                .</p>
                                <div class="uk-position-bottom-right"> <a href="{{asset('/courses-details')}}"><i class="fa fa-2x fa-angle-right"></i></a> </div>
                            </div>
                            </a>
</div>                        

</div>


  
                             
                             </div>  

               
                      <div class="uk-text-center margtp50">    <a class="uk-button uk-button-primary" href="{{asset('/courses')}}">View More</a><div>
                </div>
            </div>
        </div>
    </section>

    <section class="uk-margin-large-bottom investbg paddtpbtm investment-section">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h2 class="uk-text-center uk-margin-medium-bottom">Investment</h2>
                    <div class="uk-child-width-1-3@l uk-child-width-1-2@s uk-grid-medium uk-grid-match" data-uk-grid>
                        <div>
                            <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-book"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Mutual Fund</h6>
                                        <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-gears"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">SIP</h6>
                                                <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-area-chart"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Insurance</h6>
                                   <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-calculator"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Government Bond</h6>
                                     <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div>
                               <a href="{{asset('/investment-details')}}">
                            <div class="uk-card uk-card-default uk-card-small uk-card-body idz-resources-card">
                                <div class="uk-grid-small uk-flex-top" data-uk-grid>
                                    <div class="uk-width-auto"> <i class="fa fa-3x fa-support"></i> </div>
                                    <div class="uk-width-expand">
                                        <h6 class="uk-text-uppercase uk-text-muted uk-margin-small-bottom">Prime Portfolio</h6>
                                   <p class="uk-margin-remove-top">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
standard dummy text ever since the 1500s,</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
             <div class="uk-text-center margtp50">    <a class="uk-button uk-button-default" href="{{asset('/investment')}}">View More</a><div>
                </div>
            </div>
        </div>
    </section>
   
       <section class="uk-margin-large-bottom">
        <div class="uk-container">
            <div class="uk-grid uk-margin-medium-top">
                <div class="uk-width-1-1">
                    <h2 class="uk-margin-remove-bottom uk-text-center">What Our Clients Say ?</h2>
                    <p class="uk-margin-small-top uk-text-lead uk-text-center">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur</p>
                    <div class="uk-margin-medium-top uk-margin-medium-bottom">
                        <hr> </div>
                    <div class="uk-child-width-1-2@m uk-grid-match" data-uk-grid>
                        <div>
                            <div class="idz-testimonials">
                                 <h3 class="uk-text-center">Photo Testimonial</h3>
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s.Lorem Ipsum is simply dummy 
                                        text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s.</p>
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team1.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>David Stone</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                    
                                    <a class="uk-button uk-button-primary" href="{{asset('/Photo-testimonial')}}">View All</a>
                                    
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="idz-testimonials">
                                  <h3 class="uk-text-center">Video Testimonial</h3>
                                <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-bottom">
                                    <blockquote cite="#">
                                          <iframe width="330" height="200" class="testimonial-iframe"
                                          src="https://www.youtube.com/embed/MC6wBExX-N4" title="YouTube video player" 
                                          frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
                                          gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   
                                    </blockquote>
                                </div> <img class="uk-border-circle uk-margin-left uk-float-left" src="{{asset('public/images/content/team3.jpg')}}" alt="">
                                <div class="uk-margin-left uk-margin-small-top uk-float-left">
                                    <h6>Scott Sims</h6>
                                    <p class="uk-margin-small-top uk-text-small uk-text-uppercase">Loyal Customer</p>
                                        <a class="uk-button uk-button-secondary" href="{{asset('/video-testimonial')}}">View All</a>
                                </div>
                                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="myModal" class="modal fade anouncement-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center">
                <h2>Hey, We've got something special for you</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                


            </div>
        </div>
    </div>
</div>
    
    
    
    
    
    
   @endsection
