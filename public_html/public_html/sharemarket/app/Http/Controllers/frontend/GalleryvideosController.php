<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryvideosController extends Controller
{
    public function index(){
        return view('frontend.gallery-videos');
    }
}
