<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\frontend\HomeController;
use App\Http\Controllers\frontend\AboutController;
use App\Http\Controllers\frontend\CoursesController;
use App\Http\Controllers\frontend\FaqController;
use App\Http\Controllers\frontend\TestimonialController;
use App\Http\Controllers\frontend\BlogController;
use App\Http\Controllers\frontend\ContactController;
use App\Http\Controllers\frontend\GalleryphotosController;
use App\Http\Controllers\frontend\GalleryvideosController;
use App\Http\Controllers\frontend\EventController;
use App\Http\Controllers\frontend\PhototestimonialController;
use App\Http\Controllers\frontend\videotestimonialController;
use App\Http\Controllers\frontend\CoursesdetailsController;
use App\Http\Controllers\frontend\InvestmentdetailsController;
use App\Http\Controllers\frontend\InvestmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']); 
Route::get('/about',[AboutController::class,'index']); 
Route::get('/courses',[CoursesController::class,'index']); 
Route::get('/faq',[FaqController::class,'index']); 
Route::get('/testimonials',[TestimonialController::class,'index']);
Route::get('/blog',[BlogController::class,'index']);  
Route::get('/contact',[ContactController::class,'index']);  
Route::get('/gallery-photos',[GalleryphotosController::class,'index']); 
Route::get('/gallery-videos',[GalleryvideosController::class,'index']);  
Route::get('/event',[EventController::class,'index']);  
Route::get('/Photo-testimonial',[PhototestimonialController::class,'index']); 
Route::get('/video-testimonial',[videotestimonialController::class,'index']);
Route::get('/courses-details',[CoursesdetailsController::class,'index']);
Route::get('/investment-details',[InvestmentdetailsController::class,'index']);
Route::get('/investment',[InvestmentController::class,'index']);
