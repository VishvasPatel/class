@extends('layouts.app')
@push('styles')
<style>
         div.dataTables_paginate {
         text-align: center !important;
         float: none !important;
         margin-top: 20px;
         margin-bottom: 20px;
         }
         .center {
         text-align: center;
         }
         div.dataTables_paginate a {
         color: black !important;
         padding: 8px 16px !important;
         text-decoration: none !important;
         transition: background-color .3s !important;
         border: 1px solid #ddd !important;
         margin: 0 4px !important;
         display: inline-block;
         box-shadow: none;
         }
         div.dataTables_paginate a.active {
         background-color: #4CAF50 !important;
         color: white !important;
         border: 1px solid #4CAF50 !important;
         }
         div.dataTables_paginate a:hover:not(.active) {background-color: #ddd;}
         div.dataTables_paginate .current{   
         background-color:#0d6efd !important;
         border-color: #0d6efd !important;
         color: #fff !important;
         }
         .dataTables_wrapper .dataTables_paginate .paginate_button.current, 
         .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
         color: #fff !important;
         }
         .tablecustomscroll::-webkit-scrollbar {
         width: 1em;
         }
         .tablecustomscroll::-webkit-scrollbar-track {
         box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
         border-radius: 6px;
         }
         .tablecustomscroll::-webkit-scrollbar-thumb {
         background-color: #0d6efd;
         outline: 1px solid #0d6efd;
         border-radius: 6px;
         }
         .tablecustomscroll #example{margin-bottom: 20px;}
         @media only screen and (max-width: 479px){
         div.dataTables_paginate a{margin:5px !important;}
         .mobp{padding:15px !important;}
         .mobp .card-body {
         padding: 0px !important;
         }
         }
      </style>
@endpush

@section('content')
<div class="pagetitle">
      <h1>View Product</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">View Product</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif
 <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              
     <br> <br>
              <!-- General Form Elements -->
              <div class="col-lg-12">
               <div class="card p-4 mobp">
               <div class="card-body">
               <div class="table-responsive tablecustomscroll">
               <table id="example" class="display custom-table" style="width:100%">
               <thead>
               <tr>
                   <th width="8%">Id
               </th>
               <th width="15%">Product Name
               </th>
               <th width="15%">Brand Name</th>
               <th width="16%">Parcel Weight
</th>
               <th width="20%">Parcel Charge
</th>
               <th width="16%"> Number Of Parcel
</th>
               
               </tr>
               </thead>
               <tbody>
                   @foreach($invoiceProducts as $invoiceProduct)
                   
                   @php
                   $productName = App\Models\Product::where('id',$invoiceProduct->product_id)->first();
                   $brandname = App\Models\Brand::where('id',$invoiceProduct->brand_id)->first();
                   
                   @endphp
              <tr>
                  <td>{{$invoiceProduct->id}}</td>
                  <td>{{$productName->name}}</td>
                  <td>{{$brandname->name}}</td>
                  <td>{{$invoiceProduct->parcel_weight}}</td>
                  <td>{{$invoiceProduct->parcel_charge}}</td>
                  <td>{{$invoiceProduct->number_of_parcel}}</td>
                  
              </tr>
              @endforeach
               
               </tbody>
               </table>
               </div>
               </div>
               </div>
               </div>
              
          
          
             
      </div>
      </div>
      </div>
      </div>
      </form>
    </section>
@endsection

@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
     
@endpush



  <!-- ======= Footer ======= -->
