<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
        <li class="nav-item">
            <a class="nav-link " href="{{ url('/') }}">
                <i class="bi bi-grid"></i>
                <span>Dashbord</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{ url('create-invoice') }}">
                <i class="bi bi-grid"></i>
                <span>Create Invoice</span>
            </a>
        </li><!-- End Dashboard Nav -->
        <li class="nav-item">
            <a class="nav-link " href="{{ url('invoice-list') }}">
                <i class="bi bi-grid"></i>
                <span>Invoice List</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{ route('filter-invoice') }}">
                <i class="bi bi-grid"></i>
                <span>Filter Invoice</span>
            </a>
        </li>
        
        <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#" aria-expanded="false">
          <i class="bi bi-menu-button-wide"></i><span>Add Inventory</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-nav" class="nav-content collapse" data-bs-parent="#sidebar-nav" style="">
          <li class="nav-item">
            <a class="nav-link " href="{{ route('show-products') }}" style="color:white;">
                <span>Products</span>
            </a>
          </li>
          
          
          <!--<li class="nav-item">
            <a class="nav-link " href="{{route('show-brand')}}" style="color:white;">
                <span>Brands</span>
            </a>
          </li>-->
        </ul>
      </li>


    </ul>

</aside>