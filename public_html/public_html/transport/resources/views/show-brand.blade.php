@extends('layouts.app')

@section('content')
<div class="pagetitle">
      <h1>Brands</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Brands</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
 <section class="section">
     <a href="{{route('brands')}}" class="btn btn-primary" style="float: right">Add Brand</a>
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Brands</h5>

              <!-- Default Table -->
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($brands as $brand)
                  <tr>
                    <th scope="row">{{$brand->id}}</th>
                    <td>{{$brand->name}}</td>
                    <td><a href="{{ route('edit-brand',$brand->id)}}">Edit</a></td>
                  </tr>
                  @endforeach
                  
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>

       
      </div>
    </section>
@endsection



  <!-- ======= Footer ======= -->
