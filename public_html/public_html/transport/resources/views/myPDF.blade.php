
<!DOCTYPE html>
<html lang="mr">
   <head>
       <meta http-equiv="Content-Type" content="text-html;charset=UTF-8">
      <meta charset="utf-8">
      <title>Example 3</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <style>
   
      body {
         /*position: relative; */
        width: 21 cm;
        height: 9.9 cm;
        margin-left: auto;
        color: #001028;
        background: #FFFFFF;
       
    }
    @page {
            margin: 2px 0px 2px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }

    table tr td {
        padding: 1px;
    }
      table tr td {padding:2px; }
    table tr {
  page-break-inside: avoid;
}
 table tr td, table tr th {
        page-break-inside: avoid;
    }
    /*@page {size: 14.85cm 21cm landscape;}*/
    
   @media print{
       @page {size: A4 Portrait;}
   } 

 /*@font-face {*/
 /*           font-family: 'TiroDevanagariMarathi-Regular';*/
 /*           src: url('{{asset('assets/fonts/TiroDevanagariMarathi-Regular.ttf')}}'); */
 /*       }*/
 /*    body{*/
 /*          font-family: 'TiroDevanagariMarathi-Regular';*/
 /*       }*/
        
       .print{
           background:#002674;
           color:#fff;
           display:inline-block;
           padding:10px 20px;
           margin-top:15px;
             margin-bottom:15px;
           border:none;
           outline:none;
       } 
                @media print {
    main { page-break-after: always;}
}
main{height:100%; border: 1px solid #333;}

        
   </style>
   <body>

      <main>
    <table style="padding:5px 0px;width:100%;margin:0px auto;font-size: 12px;border-collapse: collapse;">
        <tr>
            <td style="border:1px solid #ddd;" colspan="4" rowspan="4">

                <div style="float:left;width:50%;">
                    <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:10px;">
                        पुणे न्यायालय कक्षेत

                    </p>
                    <img src="{{asset('assets/img/logo2.png')}}" width="110px">
                </div>

                <div style="float:left;width:50%;">
                    <h4 style="margin: 5px 0px; text-align:center;font-weight:700;font-size:12px;padding:0px;">|| श्री ||</h4>
                    <h2 style="margin: 5px 0px;padding:0px;font-size:12px;text-align:center;"> अनिल ट्रान्सपोर्ट

                    </h2>
                    <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:12px;text-align:center;">ट्रान्सपोर्ट कॉन्ट्रॅक्टर अँड कमिशन एजंट
                        १२५४,भवानी पेठ ,पुणे ४११०४२
                    </p>
                </div>
                <div style="clear:both;">

                </div>
                <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:12px;">पावती क्रमांक:
                    {{$invoice->receipt_number}} </p>



            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                कार्यालय क्रमांक
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%">26387674</td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                मोबाईल .
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%"> {{$invoice->mobile_number}} /  {{$invoice->mobile_number}}</td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                मोटार क्रमांक
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%">{{$invoice->lorry_number}}</td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                तारीख
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%">{{$invoice->date}}                                        </td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">माल पाठवणार</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;"  >मोबाईल</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="25%">माल घेणार </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="20%">मोबाईल</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="15%">गांव - </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">  </td>
        </tr>
        <tr>
            <td>{{$invoice->material_sender}}</td>
            <td> {{$invoice->moblie_number}} </td>
            <td>{{$invoice->material_receiver}}</td>
            <td> {{$invoice->alternate_mobile_number}} </td>
            <td>{{$invoice->city}}</td>
            <td></td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="10%">
                डाग संख्या
            </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="30%">मालाचे वर्णन</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="20%">
                ब्रँड
            </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">
                डागाचे वजन
            </td>

            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">
                डागाचे दर
            </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">
                मोटार भाडे
            </td>
        </tr>

        @foreach($products as $product)
        <tr>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->product->id}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->product->name}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->brand->name}} </td>

            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->parcel_weight}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->number_of_parcel}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->parcel_charge * $product->number_of_parcel}}</td>
        </tr>
        @endforeach
   
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">{{count($products)}}
            </td>
            <td style="border:1px solid #ddd;font-size:10px;"> एकूण डाग संख्या</td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
            </td>
            <!--<td style="border:1px solid #ddd;font-size:10px;">
                111
            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
                एकूण लॉरी शुल्क
            </td>
            <td style="border:1px solid #ddd;">
                {{$invoice->lorry_charge}}
            </td>-->
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">सेवा शुल्क</td>
            <td style="border:1px solid #ddd; ">
                {{$invoice->service_charge}}
            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
                <table style="padding:5px 0px;width:100%;margin:0px auto;font-size: 12px;border-collapse: collapse;">
                    <tbody>
                    <tr>

                        <td style="border:none;font-weight:700;font-size:9px;">
                            ऑफिस शुल्क&nbsp;:
                        </td>
                        <td style="border:none;">
                            {{$invoice->office_charge}}
                        </td>
                    </tr>
                    




        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" colspan="3">कोणती ही तक्रार ८ दिवसात कळविणे, नंतर तक्रार चालणार नाही.लिकेज झाल्यास ,वजन घटल्यास, आम्ही जबाबदार नाही .हातगाडीची हमाली वेगळी द्यावी लागेल.कुठल्याही नैसर्गिक आपत्तीला किंवा इतर प्रकारचे नुकसान झाल्यास अनिल ट्रान्सपोर्ट जबाबदार राहणार नाही.आपल्या मालाचा इन्शुरन्स मालकाने करून घ्यावा   </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:9px;">
                <table style="padding:5px 0px;width:100%;margin:0px auto;font-size: 12px;border-collapse: collapse;">
                    <tr>

                        <td style="border:none;font-weight:700;font-size:9px;">
                            पेमेंट&nbsp;:
                        </td>
                        <td style="border:none;">
                            {{$invoice->payment}}
                        </td>
                    </tr>
                </table>



            </td>
            <td style="border:1px solid #ddd;" colspan="2">
                <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:9px;">
                    स्वाक्षरी&nbsp;:
                </p>
                <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:12px;">
                    hffffffffffffffffff
                </p>


            </td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
</main>

      
   <main>
    <table style="padding:5px 0px;width:100%;margin:0px auto;font-size: 12px;border-collapse: collapse;">
        <tr>
            <td style="border:1px solid #ddd;" colspan="4" rowspan="4">

                <div style="float:left;width:50%;">
                    <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:10px;">
                        पुणे न्यायालय कक्षेत

                    </p>
                    <img src="{{asset('assets/img/logo2.png')}}" width="110px">
                </div>

                <div style="float:left;width:50%;">
                    <h4 style="margin: 5px 0px; text-align:center;font-weight:700;font-size:12px;padding:0px;">|| श्री ||</h4>
                    <h2 style="margin: 5px 0px;padding:0px;font-size:12px;text-align:center;"> अनिल ट्रान्सपोर्ट

                    </h2>
                    <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:12px;text-align:center;">ट्रान्सपोर्ट कॉन्ट्रॅक्टर अँड कमिशन एजंट
                        १२५४,भवानी पेठ ,पुणे ४११०४२
                    </p>
                </div>
                <div style="clear:both;">

                </div>
                <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;font-weight:700;font-size:12px;">पावती क्रमांक:
                    {{$invoice->receipt_number}} </p>



            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                कार्यालय क्रमांक
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%">26387674</td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                मोबाईल .
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%"> {{$invoice->mobile_number}} /  {{$invoice->mobile_number}}</td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                मोटार क्रमांक
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%">{{$invoice->lorry_number}}</td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;" width="25%">
                तारीख
            </td>
            <td style="border:1px solid #ddd;font-size:10px;" width="40%">{{$invoice->date}}                                        </td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">माल पाठवणार</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;"  >मोबाईल</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="25%">माल घेणार </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="20%">मोबाईल</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="15%">गांव - </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">  </td>
        </tr>
        <tr>
            <td>{{$invoice->material_sender}}</td>
            <td> {{$invoice->moblie_number}} </td>
            <td>{{$invoice->material_receiver}}</td>
            <td> {{$invoice->alternate_mobile_number}} </td>
            <td>{{$invoice->city}}</td>
            <td></td>
        </tr>
        <tr>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="10%">
                डाग संख्या
            </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="30%">मालाचे वर्णन</td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;" width="20%">
                ब्रँड
            </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">
                डागाचे वजन
            </td>

            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">
                डागाचे दर
            </td>
            <td style="border:1px solid #ddd; background-color: #f6f0f0;font-weight:700;font-size:9px;">
                मोटार भाडे
            </td>
        </tr>

        @foreach($products as $product)
        <tr>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->product->id}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->product->name}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->brand->name}} </td>

            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->parcel_weight}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->number_of_parcel}}</td>
            <td style="border:1px solid #ddd;font-size: 10px;font-weight:600;">{{$product->parcel_charge * $product->number_of_parcel}}</td>
        </tr>
        @endforeach
   
        <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">{{count($products)}}
            </td>
            <td style="border:1px solid #ddd;font-size:10px;"> एकूण डाग संख्या</td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
            </td>
            <!--<td style="border:1px solid #ddd;font-size:10px;">
                111
            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
                एकूण लॉरी शुल्क
            </td>
            <td style="border:1px solid #ddd;">
                {{$invoice->lorry_charge}}
            </td>-->
        </tr>
         <tr>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">सेवा शुल्क</td>
            <td style="border:1px solid #ddd; ">
                {{$invoice->service_charge}}
            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
                <table style="padding:5px 0px;width:100%;margin:0px auto;font-size: 12px;border-collapse: collapse;">
                    <tbody><tr>

                        <td style="border:none;font-weight:700;font-size:9px;">
                            ऑफिस शुल्क&nbsp;:
                        </td>
                        <td style="border:none;">
                            {{$invoice->office_charge}}
                        </td>
                    </tr>
                    </tbody></table>


            </td>
            <td style="border:1px solid #ddd;">

            </td>
            <td style="border:1px solid #ddd;font-weight:700;font-size:10px;">
                एकूण शुल्क
            </td>
            <td style="border:1px solid #ddd;">
                {{$invoice->total_charge}}
            </td>
        </tr>
    </table>
</main>

      <div style="clear:both;">

</div>  
      <button class="print" onclick="window.print()">Print this page</button>
   </body>
</html>