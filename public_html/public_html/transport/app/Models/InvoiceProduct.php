<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class InvoiceProduct extends Model
{
    use HasFactory;
    protected $table = "invoice_products";
    
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
    
    public function brand(){
        return $this->belongsTo(Brand::class,'brand_id');
    }

    
}
