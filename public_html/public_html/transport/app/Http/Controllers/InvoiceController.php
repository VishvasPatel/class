<?php


namespace App\Http\Controllers;
require 'vendor/autoload.php';

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\InvoiceProduct;
use App\Models\ProductBrand;
use Carbon\Carbon;
use PDF;
use Dompdf\Dompdf;
use Google\Cloud\Translate\V2\TranslateClient;

class InvoiceController extends Controller
{
    public function index(){
       
        
        return view('dashboard');
    }
    
    public function createInvoice(){
        $products = Product::all();
        $invoice = Invoice::orderBy('id', 'DESC')->where('is_print','=',null)->first();
        
        $invoiceProducts = [];
        if($invoice!=null){
            $invoiceProducts = InvoiceProduct::where('invoice_id',$invoice->id)->get();
        }
        
        return view('index',compact('products','invoice','invoiceProducts'));
    }
    
    public function products(){
        return view('products');
    }
    
    public function getProduct(Request $request){
       
       $products=ProductBrand::where('product_id',$request->product)->get();
       
        return response()->json([
            'status'  => true,
            'data'    => $products,
            'message' => 'Product retrieved successfully'
        ]);
    }
    
    public function submitProduct(Request $request){
        $product = Product::where('name',$request->name)->first();
        if(empty($product)){
        $product = new Product();
        $product->name = $request->name;
        $product->save();
        }
        $brandName = $request->brand_name;
        
    
        
        $brand = Brand::where('name',$brandName)->first();
        
        
        if(empty($brand)){
        $brand = new Brand();
        $brand->name = $brandName;
        $brand->product_id = $product->id;
        $brand->save();
        }
        
        $brandId = $brand->id;
        // dd($brandId,$brandName);
        $brandProduct = new ProductBrand();
        $brandProduct->product_id = $product->id;
        $brandProduct->brand_id = $brandId;
        $brandProduct->brand_name = $brandName;
        $brandProduct->save();
        
        return redirect()->route('show-products')->with('success',"Product Added Successfully");
    }
    
    public function brand(){
        $products = Product::all();
        
        return view('brands',compact('products'));
    }
    
    public function submitBrand(Request $request){
        
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->product_id = $request->product_id;
        $brand->price = $request->price;
        $brand->save();
        return redirect()->route('show-brand')->with('success',"Brand Added Successfully");
    }
    
    public function showProducts(){
        $products = Product::all();
        return view('show-product',compact('products'));
    }
    
    public function showBrand(){
        $brands = Brand::all();
        return view('show-brand',compact('brands'));
    }
    
    public function editBrand($id){
       $brand = Brand::findOrfail($id);
       $products = Product::all();
       return view('edit-brand',compact('brand','products'));
    }
    
    public function editProduct($id){
       $product = Product::findOrfail($id);
       $productBrand = ProductBrand::where('product_id',$id)->first();
       return view('edit-product',compact('product','productBrand'));
    }
    
    public function updateBrand(Request $request,$id){
        $brand = Brand::find($id);
         $brand->product_id = $request->product_id;
         $brand->name = $request->name;
        $brand->price = $request->price;
        $brand->save();
        return redirect()->route('show-brand')->with('success',"Brand Added Successfully");
    }
    
    public function updateProduct(Request $request,$id){
        $product = Product::find($id);
        $product->name = $request->name;
        $product->save();
        $brandName = $request->brand_name;
       
        
        $brand = Brand::where('name',$brandName)->first();
        
        
        if(empty($brand)){
        $brand = new Brand();
        $brand->name = $brandName;
        $brand->product_id = $product->id;
        $brand->save();
        }
        
        $brandId = $brand->id;
        // dd($brandId,$brandName);
        $brandProduct = ProductBrand::where('product_id',$product->id)->first();
        $brandProduct->product_id = $product->id;
        $brandProduct->brand_id = $brandId;
        $brandProduct->brand_name = $brandName;
        $brandProduct->save();
        
        
        
         return redirect()->route('show-products')->with('success',"Product Added Successfully");
    }
    
    public function submitDetails(Request $request){
        
        $invoice = Invoice::orderBy('id', 'DESC')->where('is_print','=',null)->first();
        /*$translate = new TranslateClient([
    'key' => 'AIzaSyAoAmbinl0xsFID5Uk0xuR-ATatTKU2zHs'
]);

// Translate text from english to french.
$result = $translate->translate('Hello world!', [
    'target' => 'mr'
]);
dd($result);*/
        
        if(empty($invoice)){
            $invoice = new Invoice();
            $invoice->receipt_number = time();
            $invoice->print_invoice = $request->print_invoice;
            $invoice->lorry_number = $request->lorry_number;
            $invoice->date = Carbon::now();
            $invoice->material_sender = $request->material_sender;
            $invoice->material_receiver = $request->material_receiver;
            $invoice->mobile_number = $request->mobile_number;
            $invoice->alternate_mobile_number = $request->alternate_mobile_number;
            $invoice->city = $request->city;
            $invoice->address = $request->address;
            $invoice->service_charge = 2;
            $invoice->save();
        } else {
            $invoice->print_invoice = $request->print_invoice;
            $invoice->lorry_number = $request->lorry_number;
            $invoice->material_sender = $request->material_sender;
            $invoice->material_receiver = $request->material_receiver;
            $invoice->mobile_number = $request->mobile_number;
            $invoice->alternate_mobile_number = $request->alternate_mobile_number;
            $invoice->city = $request->city;
            $invoice->address = $request->address;
            $invoice->save();
        }
        if($request->product_id!=null && $request->brand_id!=null && $request->parcel_weight!=null && $request->parcel_charge!=null && $request->number_of_parcel!=null){
         $product = Product::find($request->product_id);
            $brand = Brand::find($request->brand_id);

            if(empty($product)){
                $product = new Product();
                $product->name = $request->product_id;
                $product->save();
            }

            if(empty($brand)){
                $brand = new Brand();
                $brand->name = $brandName;
                $brand->product_id = $product->id;
                $brand->save();
            }
            // dd($product,$brand);
            $brandId = $brand->id;
            $productBrand = ProductBrand::where('product_id',$product->id)->where('brand_id',$request->brand_id)->first();
            // dd($brandId,$brandName);
            if($productBrand==null){
                $brandProduct = new ProductBrand();
                $brandProduct->product_id = $product->id;
                $brandProduct->brand_id = $brandId;
                $brandProduct->brand_name = $brand->name;
                $brandProduct->save();
            }
        
        $invoiceProduct = new InvoiceProduct();
        $invoiceProduct->invoice_id = $invoice->id;
         $invoiceProduct->product_id = $request->product_id;
        $invoiceProduct->brand_id = $request->brand_id;
        $invoiceProduct->date = Carbon::now();
        $invoiceProduct->parcel_weight = $request->parcel_weight;
         $invoiceProduct->parcel_charge = $request->parcel_charge;
         $invoiceProduct->number_of_parcel = $request->number_of_parcel;
         $invoiceProduct->save();
         $lorryCharge = $request->parcel_charge * $request->number_of_parcel;
        $invoice->lorry_charge = $invoice->lorry_charge + $lorryCharge;
        $totalParcel = $invoice->total_number_of_parcel + $request->number_of_parcel;
        $invoice->total_number_of_parcel = $totalParcel;
        $invoice->save();
        
        if($invoice->office_charge==null){
            $officeCharge = 0;
        } else {
            $officeCharge = $invoice->office_charge;
        }
        
        $invoice->total_charge = $invoice->lorry_charge + $invoice->service_charge + $officeCharge;
        $invoice->save();
        }
         
        
        
        return redirect()->back()->with('success',"Invoice Data Stored Successfully");
        
    }
    
    public function submitCharge(Request $request){
       $invoice = Invoice::orderBy('id', 'DESC')->where('is_print','=',null)->first();
     
       if(empty($invoice)){
           return redirect()->back()->with('error','First Fill up above details');
       }
       $invoice->is_submit_invoice = 1;
       $invoice->office_charge = $request->office_charge;
       $invoice->total_charge = $invoice->total_charge + $request->office_charge;
       $invoice->save();
       return redirect()->back()->with('success',"Invoice Data Stored Successfully");
    }
    
    public function deleteProduct($id){
        $invoiceProduct = InvoiceProduct::find($id);
        $invoiceProduct->delete();
        return redirect()->back()->with('success',"Product Deleted Successfully");
    }
    
    public function updateInvoice(Request $request){
        
        $invoiceProduct = InvoiceProduct::find($request->id);
        $invoiceProduct->product_id = $request->product_id;
        $invoiceProduct->brand_id = $request->brand_id;
        $invoiceProduct->parcel_weight = $request->parcel_weight;
         $invoiceProduct->parcel_charge = $request->parcel_charge;
         $invoiceProduct->number_of_parcel = $request->number_of_parcel;
         $invoiceProduct->save();
         
         $invoice = Invoice::find($invoiceProduct->invoice_id);
         $product = InvoiceProduct::where('invoice_id',$invoiceProduct->invoice_id)->get();
         $totalParcel = 0;
         $totalLorryCharge = 0;
         
         foreach($product as $p){
             $totalParcel+=$p->number_of_parcel;
             $lorry = $request->parcel_charge * $request->number_of_parcel;
             $totalLorryCharge+=$lorry;
         }
        
         $invoice->total_number_of_parcel = $totalParcel;
         $invoice->lorry_charge = $totalLorryCharge;
         $invoice->save();
         
         $invoice->total_charge = $invoice->lorry_charge + $invoice->service_charge;
         $invoice->save();
         return response()->json([
            'status'  => true,
            'message' => 'Data Updated Successfully'
        ]);
    }
    
    public function getOfficeCharge(Request $request){
        
        $invoice = Invoice::orderBy('id', 'DESC')->where('is_print','=',null)->first();
       $totalCharge = $invoice->total_charge + $request->office;
       return response()->json([
            'status'  => true,
            'data' => $totalCharge
        ]);
    }
    
    public function updatePDF(Request $request,$id){
        $invoice = Invoice::find($id);
        
        if($request->office_charge==null){
             $officeChrge = 0;
         } else if($request->office_charge > $invoice->office_charge){
             $officeChrge = $request->office_charge - $invoice->office_charge;
         } else if($invoice->office_charge > $request->office_charge){
             $officeChrge = $invoice->office_charge - $request->office_charge;
         } else {
             $officeChrge = 0;
         }
         $invoice->office_charge = $request->office_charge;
         $invoice->payment = $request->payment;
         $invoice->save();
         
         $totalCharge = $officeChrge + $invoice->total_charge;
         $invoice->total_charge = $totalCharge;
         $invoice->is_print = 1;
         $invoice->save();
         $products = [];
        if(!empty($invoice)){
            $products = InvoiceProduct::with('product','brand')->where('invoice_id',$invoice->id)->get();
        }
        
        return view('myPDF',compact('invoice','products'));
    }
    
    public function generatePDF(Request $request){
        
         $invoice = Invoice::orderBy('id', 'DESC')->where('is_print','=',null)->first();
         
         if($request->office_charge==null){
             $officeChrge = 0;
         } else {
             $officeChrge = $request->office_charge;
         }
        //   dd($invoice);
         $invoice->office_charge = $officeChrge;
         $invoice->payment = $request->payment;
         $invoice->save();
         
         $totalCharge = $officeChrge + $invoice->total_charge;
         $invoice->total_charge = $totalCharge;
         $invoice->is_print = 1;
         $invoice->save();
         $products = [];
        if(!empty($invoice)){
            $products = InvoiceProduct::with('product','brand')->where('invoice_id',$invoice->id)->get();
        }
        if($invoice->print_invoice=="कार्यालय")
        {
            // dd("hiii");
            return view('pdf-potrait',compact('invoice','products'));
        } else {
            // dd("hello");
            return view('myPDF',compact('invoice','products'));
        }
        // dd("welcome");
        
        //  $customPaper = array(0,0,830,1440);
        $pdf = PDF::loadView('myPDF', compact('invoice','products'))->setPaper("a4", 'landscape');
        // $pdf->setOptions(['dpi' => 72 ]);
        return $pdf->download('transport.pdf');
        
    }
    
    public function invoiceList(){
        $invoices = Invoice::all();
        return view('invoice-list',compact('invoices'));
        
    }
    
    public function filterInvoice(){
        $invoices = Invoice::all();
        return view('filter-invoice',compact('invoices'));
    }
    
    public function editInvoice($id){
        $products = Product::all();
        $invoice = Invoice::find($id);
        
        $invoiceProducts = [];
        if($invoice!=null){
            $invoiceProducts = InvoiceProduct::where('invoice_id',$invoice->id)->get();
        }
        
        return view('edit-invoice',compact('products','invoice','invoiceProducts'));
    }
    
    public function updateDetails(Request $request,$id){
        $invoice = Invoice::find($id);
        /*$translate = new TranslateClient([
    'key' => 'AIzaSyAoAmbinl0xsFID5Uk0xuR-ATatTKU2zHs'
]);

// Translate text from english to french.
$result = $translate->translate('Hello world!', [
    'target' => 'mr'
]);
dd($result);*/
        
        if(empty($invoice)){
            return redirect()->back()->with('error',"Invoice Not Found");
        } else {
            $invoice->lorry_number = $request->lorry_number;
            $invoice->material_sender = $request->material_sender;
            $invoice->material_receiver = $request->material_receiver;
            $invoice->mobile_number = $request->mobile_number;
            $invoice->alternate_mobile_number = $request->alternate_mobile_number;
            $invoice->city = $request->city;
            $invoice->address = $request->address;
            $invoice->save();
        }
        if($request->product_id!=null && $request->brand_id!=null && $request->parcel_weight!=null && $request->parcel_charge!=null && $request->number_of_parcel!=null){
        if(empty($product)){
                $product = new Product();
                $product->name = $request->product_id;
                $product->save();
            }

            if(empty($brand)){
                $brand = new Brand();
                $brand->name = $brandName;
                $brand->product_id = $product->id;
                $brand->save();
            }
            // dd($product,$brand);
            $brandId = $brand->id;
            $productBrand = ProductBrand::where('product_id',$product->id)->where('brand_id',$request->brand_id)->first();
            // dd($brandId,$brandName);
            if($productBrand==null){
                $brandProduct = new ProductBrand();
                $brandProduct->product_id = $product->id;
                $brandProduct->brand_id = $brandId;
                $brandProduct->brand_name = $brand->name;
                $brandProduct->save();
            }
        $invoiceProduct = new InvoiceProduct();
        $invoiceProduct->invoice_id = $invoice->id;
         $invoiceProduct->product_id = $request->product_id;
        $invoiceProduct->brand_id = $request->brand_id;
        $invoiceProduct->date = Carbon::now();
        $invoiceProduct->parcel_weight = $request->parcel_weight;
         $invoiceProduct->parcel_charge = $request->parcel_charge;
         $invoiceProduct->number_of_parcel = $request->number_of_parcel;
         $invoiceProduct->save();
         $lorryCharge = $request->parcel_charge * $request->number_of_parcel;
        $invoice->lorry_charge = $invoice->lorry_charge + $lorryCharge;
        $totalParcel = $invoice->total_number_of_parcel + $request->number_of_parcel;
        $invoice->total_number_of_parcel = $totalParcel;
        $invoice->save();
        
        if($invoice->office_charge==null){
            $officeCharge = 0;
        } else {
            $officeCharge = $invoice->office_charge;
        }
        
        $invoice->total_charge = $invoice->lorry_charge + $invoice->service_charge + $officeCharge;
        $invoice->save();
        }
         
        
        
        return redirect()->back()->with('success',"Invoice Data Updated Successfully");
    }
    
    public function filterData(Request $request){
         
        $invoices = Invoice::with('invoiceProduct');
        if($request->lorry_number!=null){
           $invoices->where('lorry_number',$request->lorry_number);
        }
        
        if($request->material_sender!=null){
           $invoices->where('material_sender',$request->material_sender);
        }
        if($request->material_receiver!=null){
           $invoices->where('material_receiver',$request->material_receiver);
        }
        
        if($request->start_date!=null){
           $invoices->whereDate('date','>',$request->start_date);
        }
        
        if($request->end_date!=null){
           $invoices->whereDate('date','<',$request->end_date);
        }
        if($request->city!=null){
           $invoices->where('city','=',$request->city);
        }
        
        if($request->payment!=null){
           $invoices->where('payment','=',$request->payment);
        }
        
        $invoices = $invoices->get();
        
        $totalAmount = 0;
        if(count($invoices)>0){
            foreach($invoices as $invoice){
                $totalAmount+=$invoice->total_charge;
            }
        }
       
        
        return view('invoice-fetch-data',compact('invoices','totalAmount'));
        
        
    }
    
    public function viewProduct($id){
        
        $invoiceProducts = InvoiceProduct::where('invoice_id',$id)->get();
        return view('view-product',compact('invoiceProducts'));
    }
}
