@extends('layouts.app')
@push('styles')
<style>
         div.dataTables_paginate {
         text-align: center !important;
         float: none !important;
         margin-top: 20px;
         margin-bottom: 20px;
         }
         .center {
         text-align: center;
         }
         div.dataTables_paginate a {
         color: black !important;
         padding: 8px 16px !important;
         text-decoration: none !important;
         transition: background-color .3s !important;
         border: 1px solid #ddd !important;
         margin: 0 4px !important;
         display: inline-block;
         box-shadow: none;
         }
         div.dataTables_paginate a.active {
         background-color: #4CAF50 !important;
         color: white !important;
         border: 1px solid #4CAF50 !important;
         }
         div.dataTables_paginate a:hover:not(.active) {background-color: #ddd;}
         div.dataTables_paginate .current{   
         background-color:#0d6efd !important;
         border-color: #0d6efd !important;
         color: #fff !important;
         }
         .dataTables_wrapper .dataTables_paginate .paginate_button.current, 
         .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
         color: #fff !important;
         }
         .tablecustomscroll::-webkit-scrollbar {
         width: 1em;
         }
         .tablecustomscroll::-webkit-scrollbar-track {
         box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
         border-radius: 6px;
         }
         .tablecustomscroll::-webkit-scrollbar-thumb {
         background-color: #0d6efd;
         outline: 1px solid #0d6efd;
         border-radius: 6px;
         }
         .tablecustomscroll #example{margin-bottom: 20px;}
         @media only screen and (max-width: 479px){
         div.dataTables_paginate a{margin:5px !important;}
         .mobp{padding:15px !important;}
         .mobp .card-body {
         padding: 0px !important;
         }
         }
      </style>
@endpush

@section('content')
<div class="pagetitle">
      <h1>Edit Invoice</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Edit Invoice</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif
 <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              
     <br> <br>
              <!-- General Form Elements -->
              <form method="post" action="{{ route('update-details',$invoice->id) }}">
                  @csrf
                   <div class="row">
                       <div class="col-lg-6">
                           <div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">जागा</label>
                  <div class="col-sm-8">
                    <select class="form-control js-example-tags" name="print_invoice">
                        <option value="कार्यालय" {{ @$invoice->print_invoice=="कार्यालय" ? "selected" : "" }}>कार्यालय</option>
<option value="बाजार" {{ @$invoice->print_invoice=="बाजार" ? "selected" : "" }}>बाजार</option>
                    </select>
                  </div>
                </div>
                           
                <div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">मोटार क्रमांक</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="lorry_number" @if(isset($invoice->lorry_number)) value="{{ $invoice->lorry_number }}"   @endif>
                  </div>
                </div>
                          
               
                <div class="row mb-3">
                  <label for="inputPassword" class="col-sm-4 col-form-label">माल पाठवणार</label>
                  <div class="col-sm-8">
                                        
                           
                                
                                                                   <input type="text" class="form-control" name="material_sender" @if(isset($invoice->material_sender)) value="{{ $invoice->material_sender }}"   @endif>
                               
                               
                          
                            
                  </div>
                </div>
                           
                            <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">माल घेणार</label>
                  <div class="col-sm-8">
                      
                           
                               
                                                  <input type="text" class="form-control" name="material_receiver" @if(isset($invoice->material_sender)) value="{{ $invoice->material_receiver }}"   @endif>

                               
                               
                          
                          
                   </div>
                </div>
              
               
                       </div>
                       
                        <div class="col-lg-6">
                            
                            
                
                              <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">मोबाईल</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" onkeypress="return isNumber(event)" id="mobile_number" maxlength="10"  name="mobile_number" @if(isset($invoice->mobile_number)) value="{{ $invoice->mobile_number }}"   @endif>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">पर्यायी मोबाईल


</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" onkeypress="return isNumber(event)" maxlength="10" id="alternate_mobile_number" name="alternate_mobile_number" @if(isset($invoice->alternate_mobile_number)) value="{{ $invoice->alternate_mobile_number }}"   @endif>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="city" class="col-sm-4 col-form-label">गांव</label>
                  <div class="col-sm-8">
                  <select name="city" id="city" class="form-control js-example-tags">
                  
                     <option value="" selected hidden>Select</option> 
                   <option value="सासवड" {{ @$invoice->city=="सासवड" ? "selected" : "" }}>   सासवड</option>
<option value="जेजुरी" {{ @$invoice->city=="जेजुरी" ? "selected" : "" }}>जेजुरी</option>
<option value="मोरगाव" {{ @$invoice->city=="मोरगाव" ? "selected" : "" }}> मोरगाव </option>
<option value="वालहा" {{ @$invoice->city=="वालहा" ? "selected" : "" }}> वालहा </option>
<option value="ढालेवाडी" {{ @$invoice->city=="ढालेवाडी" ? "selected" : "" }}> ढालेवाडी </option>
<option value="कोथाळे " {{ @$invoice->city=="कोथाळे " ? "selected" : "" }}> कोथाळे  </option>
<option value="धालेवाडी" {{ @$invoice->city=="धालेवाडी" ? "selected" : "" }}> धालेवाडी </option>
<option value="कोलविहरे" {{ @$invoice->city=="कोलविहरे" ? "selected" : "" }}> कोलविहरे </option>
<option value="शिवरी" {{ @$invoice->city=="शिवरी" ? "selected" : "" }}> शिवरी</option>
<option value="भिवरी" {{ @$invoice->city=="भिवरी" ? "selected" : "" }}> भिवरी </option>
<option value="भिवडी" {{ @$invoice->city=="भिवडी" ? "selected" : "" }}> भिवडी </option>
<option value="वीर" {{ @$invoice->city=="वीर" ? "selected" : "" }}> वीर</option>
<option value="हिवरे" {{ @$invoice->city=="हिवरे" ? "selected" : "" }}> हिवरे</option>
<option value="बोपगाव" {{ @$invoice->city=="बोपगाव" ? "selected" : "" }}> बोपगाव</option>
<option value="चांबळी" {{ @$invoice->city=="चांबळी" ? "selected" : "" }}> चांबळी </option>
<option value="झेंडेवाडी " {{ @$invoice->city=="झेंडेवाडी " ? "selected" : "" }}> झेंडेवाडी  </option>
<option value="काळेवाडी" {{ @$invoice->city=="काळेवाडी" ? "selected" : "" }}> काळेवाडी </option>
<option value="दिवे" {{ @$invoice->city=="दिवे" ? "selected" : "" }}> दिवे </option>
<option value="खळद " {{ @$invoice->city=="खळद " ? "selected" : "" }}> खळद  </option>
<option value="नारायणपूर" {{ @$invoice->city=="नारायणपूर " ? "selected" : "" }}> नारायणपूर  </option>
<option value="नारायणपेठ" {{ @$invoice->city=="नारायणपेठ " ? "selected" : "" }}> नारायणपेठ  </option>
<option value="पवारवाडी " {{ @$invoice->city=="पवारवाडी " ? "selected" : "" }}> पवारवाडी  </option>
<option value="वाघापूर" {{ @$invoice->city=="वाघापूर" ? "selected" : "" }}> वाघापूर </option>
<option value="सिंगापूर " {{ @$invoice->city=="सिंगापूर " ? "selected" : "" }}> सिंगापूर  </option>
<option value="दौंडज" {{ @$invoice->city=="दौंडज " ? "selected" : "" }}> दौंडज </option>
 
                      
</select>
                  </div>
                </div>
                
                <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">पत्ता</label>
                  <div class="col-sm-8">
                    <textarea name="address" id="address"  class="form-control" @if(isset($invoice->address))   @endif>{{ @$invoice->address ?  $invoice->address : ""}}</textarea>
                  </div>
                </div>
                             
                
               
               

                       </div>
                  </div>
                
              
               

                

        

            </div>
          </div>

        </div>

        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <br> <br>

              <!-- Advanced Form Elements -->
              
                 <div class="row">
                       <div class="col-lg-6">
                             <div class="row mb-3">
                  <label class="col-sm-4 col-form-label">मालाचे वर्णन</label>
                  <div class="col-sm-8">
                    <select class="form-control js-example-tags" name="product_id" id="product">
                        <option value="" seleted hidden>Select</option>
                        @foreach($products as $product)
                        <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                  
                  
                    <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">ब्रँड   </label>
                  <div class="col-sm-8">
                    <select class="form-control js-example-tags"  name="brand_id" id="brand">
                        
                    </select>
                  </div>
                </div>
                           
                              <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">डागाचे वजन</label>
                  <div class="col-sm-8">
                    <input type="number"  class="form-control" name="parcel_weight">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputNumber"  class="col-sm-4 col-form-label">डागाचे दर</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="parcel_charge">
                  </div>
                </div>
                           <div class="row mb-3">
                  <label for="inputNumber"  class="col-sm-4 col-form-label">डाग संख्या </label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="number_of_parcel">
                  </div>
                </div> 
                <div class="row mb-3">
               
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit Details</button>
                  </div>
                </div>
                       <div class="row mb-3">
                  <label class="col-sm-4 col-form-label" name="total_number_of_parcel"> एकूण डाग </label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="total_number_of_parcel" value="{{ @$invoice->total_number_of_parcel ? $invoice->total_number_of_parcel : 0 }}" disabled="disabled">
                  </div>
                </div>      

                     </div>
                     </form>
                     
                     <div class="col-lg-6">
                         <form method="post" action="{{ route('update-pdf',$invoice->id) }}">
                             @csrf
                      <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-4 col-form-label">मोटार भाडे </label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" name="lorry_charge" value="{{ @$invoice->lorry_charge ? $invoice->lorry_charge : 0 }}" disabled="disabled">
                  </div>
                </div>
                
                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-4 col-form-label">ऑफिस शुल्क</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" value={{$invoice->office_charge}} name="office_charge" id="office_charge" required>
                  </div>
                </div>
                         
                       <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-4 col-form-label">सेवा शुल्क</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="service_charge" value="2" disabled="disabled">
                  </div>
                </div>
                  
                  
                    
                
                             <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-4 col-form-label">एकूण</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="total_charge" name="total_charge" value="{{ @$invoice->total_charge ? $invoice->total_charge : 0 }}" disabled="disabled">
                  </div>
                </div>
                                                 <div class="row mb-3">
                  <label class="col-sm-4 col-form-label" name="total_number_of_parcel"> पेमेंट </label>
                  <div class="col-sm-8">
                        <select name="payment" id="payment" class="form-control"  required>
                  
 <option value="paid" {{ @$invoice->payment=="paid" ? "selected" : "" }}>Paid</option>
  <option value="topay" {{ @$invoice->payment=="unpaid" ? "selected" : "" }}>Topay</option>
</select>
                  </div>
                </div>
                
                
                
                
                
            
               
                       
                  
                 
                     
                     </div>
                    
                  </div>
                           
                           
                        

            </div>
          </div>

        </div>
        
        
           
        <div class="col-lg-12">
               <div class="card p-4 mobp">
               <div class="card-body">
               <div class="table-responsive tablecustomscroll">
               <table id="example" class="display" style="width:100%">
               <thead>
               <tr>
               <th width="30%">मालाचे वर्णन
               </th>
               <th width="5%">ब्रँड </th>
               <th width="10%"> डागाचे वजन 
               </th>
               <th>डागाचे दर
               </th>
               <th> डाग संख्या 
               </th>
               <th>कृती
               </th>
               </tr>
               </thead>
               <tbody>
               @if(count($invoiceProducts)>0)
    @foreach($invoiceProducts as $invoiceProduct)
        <tr>
            <td><select class="form-control js-example-tags" onchange="getBrand({{$invoiceProduct->id}})" id="product_id_{{$invoiceProduct->id}}" disabled="disabled"  name="product_id" id="product">
                    <option value=""  hidden="">Select</option>
                    @foreach($products as $product)
                        <option value="{{ $product->id }}" {{ $product->id==$invoiceProduct->product_id ? "selected" : "" }}>{{ $product->name }}</option>
                    @endforeach
                </select></td>
                @php
            $brands = App\Models\ProductBrand::where('product_id',$invoiceProduct->product_id)->get();
            @endphp
            <td><select class="form-control js-example-tags"  disabled="disabled" id="brand_id_{{$invoiceProduct->id}}"  name="brand_id" id="brand">
                <option value="" selected hidden>Select</option>
                @foreach($brands as $brand)
                        <option value="{{ $brand->brand_id }}" {{ $brand->brand_id==$invoiceProduct->brand_id ? "selected" : "" }}>{{ $brand->brand_name }}</option>
                    @endforeach
                </select></td>
            <td><input type="number" disabled="disabled"   class="form-control" id="parcel_weight_{{$invoiceProduct->id}}" name="parcel_weight" value="{{$invoiceProduct->parcel_weight}}"></td>
            <td><input type="number" disabled="disabled" class="form-control" id="parcel_charge_{{$invoiceProduct->id}}" name="parcel_charge"  value="{{$invoiceProduct->parcel_charge}}"></td>
            <td><input type="number" disabled="disabled" class="form-control" id="number_of_parcel_{{$invoiceProduct->id}}" name="number_of_parcel" value="{{$invoiceProduct->number_of_parcel}}"></td>
            <td><button type="button" id="editbtn_{{$invoiceProduct->id}}"  class="btn btn-primary" onclick="disabledField({{$invoiceProduct->id}})">Edit</button><button type="button" onclick="updateField({{$invoiceProduct->id}})" id="updatebtn_{{$invoiceProduct->id}}" class="btn btn-warning" style="display:none">Update</button><a href="{{ route('delete-product',$invoiceProduct->id) }}" class="btn btn-danger">Delete</a></td>
        </tr>
    @endforeach
@endif
               
               </tbody>
               </table>
               </div>
               </div>
               </div>
               </div>
              
          
          
             <div class="row mb-3">
               
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-primary">Print Invoice</button>
                  </div>
                </div>
      </div>
      </form>
    </section>
@endsection

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
  $("#product").change(function()
{
	var product=$("#product").val();
	$.ajax({
                url: "{{route('get-product')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "product": product
                },
                success: function(responce)
		{
			jQuery('#brand').empty();
			var data = responce.data;
			
			
			$.each(data, function (index, value) {
                
                $('#brand').append('<option value="' + value.brand_id + '">' + value.brand_name + '</option>');
			});

		}, 
            });
});
});


</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function () {
         $('#example').DataTable({
         pagingType: 'full_numbers',
         "bInfo" : false,
         "lengthChange": false, 
         "aoColumns": [
         { sWidth: '38%' },
         { sWidth: '14%' },
         { sWidth: '16%' },
         { sWidth: '12%' },
         { sWidth: '10%' },
         { sWidth: '10%' },
         ],
         "searching": false
         });
         });
         
      </script>
      <script>
          function disabledField(id){
             $("#product_id_"+ id).prop("disabled",false);
             $("#brand_id_"+ id).prop("disabled",false);
             $("#parcel_weight_"+ id).prop("disabled",false);
             $("#parcel_charge_"+ id).prop("disabled",false);
             $("#number_of_parcel_"+ id).prop("disabled",false);
             $("#editbtn_"+ id).css("display","none");
             $("#updatebtn_"+ id).css("display","block");
          }
          
          function updateField(id){
            var product_id =  $("#product_id_"+ id).val();
           var brand_id =  $("#brand_id_"+ id).val();
            var parcel_weight = $("#parcel_weight_"+ id).val();
            var parcel_charge = $("#parcel_charge_"+ id).val();
            var number_of_parcel =  $("#number_of_parcel_"+ id).val();
            
             $.ajax({
                url: "{{route('update-invoice')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" : id,
                    "product_id": product_id,
                    "brand_id": brand_id,
                    "parcel_weight": parcel_weight,
                    "parcel_charge": parcel_charge,
                    "number_of_parcel": number_of_parcel,
                    
                },
                success: function (response) {
                    if (response.status === true) {
                        window.location.reload();

                    }

                }, error: function (error) {

                }
            });
           
            
          }
      </script>
      <script>
          function getBrand(id){
              var product = $("#product_id_"+id).val();
              
              $.ajax({
                url: "{{route('get-product')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "product": product
                },
                success: function(responce)
		{
			jQuery('#brand_id_'+id).empty();
			var data = responce.data;
			
			
			$.each(data, function (index, value) {
                
                $('#brand_id_'+id).append('<option value="' + value.brand_id + '">' + value.brand_name + '</option>');
			});

		}, 
            });
          }
      </script>
      <script>
         function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
      </script>
      <script>
          $("#office_charge").on('input',function(){
              var office = $("#office_charge").val();
               $.ajax({
                url: "{{route('get-office-charge')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "office": office
                },
                success: function(responce)
		{
			if (responce.status === true) {
                        $("#total_charge").val(responce.data);

                    }

		}, 
            });
              
          })
      </script>
      
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
$(document).ready(function(){
$(".js-example-tags").select2({
tags: true
});
});
</script>   
      
      
@endpush



  <!-- ======= Footer ======= -->
