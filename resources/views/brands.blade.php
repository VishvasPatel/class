@extends('layouts.app')

@section('content')
<div class="pagetitle">
      <h1>Create Brand</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Create Brand</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
 <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              
     <br> <br>
              <!-- General Form Elements -->
              <form method="post" action="{{route('submit-brand')}}">
                  @csrf
                   <div class="row">
                       <div class="col-lg-6">
                           
                           <div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">Product</label>
                  <div class="col-sm-8">
                    <select class="form-control" name="product_id" required>
                        <option value="" selected hidden>Select</option>
                        @foreach($products as $product)
                        <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                           
                <div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">Brand Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" required>
                  </div>
                </div>
                          
               
                          
              
               
                       </div>
                       
                  </div>
                
              
               

                <div class="row mb-3">
               
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>

         
             
      </div>
    </section>
@endsection



  <!-- ======= Footer ======= -->
