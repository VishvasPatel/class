<!DOCTYPE html>
<html lang="mr">
   <head>
       <meta http-equiv="Content-Type" content="text-html;charset=UTF-8">
      <meta charset="utf-8">
      <title>Example 3</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <style>
   
      body {
         /*position: relative; */
        width: 14.8cm;
        height: 21cm;
        margin-left: auto;
        color: #001028;
        background: #FFFFFF;
        border: 1px solid #333;
    }
    @page {
            margin: 2px 0px 2px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }

    table tr td {
        padding: 1px;
    }
      table tr td {padding:2px; }
    table tr {
  page-break-inside: avoid;
}
 table tr td, table tr th {
        page-break-inside: avoid;
    }
    /*@page {size: 14.85cm 21cm landscape;}*/
    
 @font-face {
            font-family: 'TiroDevanagariMarathi-Regular';
            src: url('{{asset('assets/fonts/TiroDevanagariMarathi-Regular.ttf')}}'); 
        }
     body{
           font-family: 'TiroDevanagariMarathi-Regular';
        }
        
   </style>
   <body>

      <main>
         <table style="font-family: 'TiroDevanagariMarathi-Regular';padding:5px 0px;width:100%;margin:0px auto;font-size: 12px;border-collapse: collapse;">
            <tr>
               <td style="border:1px solid #ddd;" colspan="4" rowspan="4">
                  <img src="{{asset('assets/img/logo2.png')}}" width="150px"><br>
                  <h4 style="margin: 5px 0px; text-align:center;font-weight:400;padding:0px;">|| श्री ||</h4>
                अनिल ट्रान्सपोर्ट 
 
 </h2>
                  <p style="margin: 5px 0px;padding:0px;margin-bottom:0px;">ट्रान्सपोर्ट कॉन्ट्रॅक्टर अँड कमिशन एजंट 
१२५४,भवानी पेठ ,पुणे ४११०४२ </p>
               </td>
               <td style="border:1px solid #ddd;" width="25%">
                  कार्यालय क्रमांक
               </td>
               <td style="border:1px solid #ddd;" width="40%">26387674</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;" width="25%">
                  मोबाईल . 
               </td>
               <td style="border:1px solid #ddd;" width="40%"> 9970705454 / 8530855454</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;" width="25%">
                  मोटार क्रमांक 
               </td>
               <td style="border:1px solid #ddd;" width="40%">{{$invoice->lorry_number}}</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;" width="25%">
                  तारीख
               </td>
               <td style="border:1px solid #ddd;" width="40%">{{ \Carbon\Carbon::now()->format('Y-m-d') }}                                          </td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">पावती क्रमांक</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;"  >माल पाठवणार</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">मोबाईल</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;" width="30%">माल घेणार </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">गांव - </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">Court</td>
            </tr>
            <tr>
               <td>{{$invoice->receipt_number}}</td>
               <td>{{$invoice->material_sender}}  </td>
               <td>{{$invoice->mobile_number}}</td>
               <td>{{$invoice->material_receiver}}</td>
               <td>{{$invoice->city}}</td>
               <td>Pune Court</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;" width="30%">मालाचे वर्णन</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;" width="20%">
                  ब्रँड 
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">
                  डागाचे वजन
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">
                  डाग संख्या
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">
                  डागाचे दर
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0;">
                  मोटार भाडे
               </td>
            </tr>
            
            @foreach($products as $product)
            <tr>
               <td style="border:1px solid #ddd;padding-top:0px;padding-bottom:0px;">{{$product->product->name}}</td>
               <td style="border:1px solid #ddd;padding-top:0px;padding-bottom:0px;">{{$product->brand->name}}</td>
               <td style="border:1px solid #ddd;padding-top:0px;padding-bottom:0px;">{{$product->parcel_weight}}</td>
               <td style="border:1px solid #ddd;padding-top:0px;padding-bottom:0px;">{{$product->number_of_parcel}}</td>
               <td style="border:1px solid #ddd;padding-top:0px;padding-bottom:0px;">{{$product->parcel_charge}}</td>
               <td style="border:1px solid #ddd;padding-top:0px;padding-bottom:0px;">{{$product->parcel_charge * $product->number_of_parcel}}</td>
            </tr>
            @endforeach
            <tr>
               <td style="border:1px solid #ddd;">ऑफिस शुल्क
               </td>
               <td style="border:1px solid #ddd;"> {{$invoice->office_charge}}</td>
               <td style="border:1px solid #ddd;">
                  एकूण पार्सल संख्या                </td>
               <td style="border:1px solid #ddd;">
                  {{$invoice->total_number_of_parcel}}
               </td>
               <td style="border:1px solid #ddd;">
                  एकूण लॉरी शुल्क
               </td>
               <td style="border:1px solid #ddd;">
                   {{$invoice->lorry_charge}}
               </td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;">सेवा शुल्क</td>
               <td style="border:1px solid #ddd; ">
                 {{$invoice->service_charge}}
               </td>
               <td style="border:1px solid #ddd;">
                 पेमेंट
               </td>
               <td style="border:1px solid #ddd;">
                 
               </td>
               <td style="border:1px solid #ddd;">
                  एकूण शुल्क
               </td>
               <td style="border:1px solid #ddd;">
                   {{$invoice->total_charge}}
               </td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;" colspan="3">  कोणती ही तक्रार ८ दिवसात कळविणे, नंतर तक्रार चालणार नाही.लिकेज झाल्यास ,वजन घटल्यास, आम्ही जबाबदार नाही .हातगाडीची हमाली वेगळी द्यावी लागेल.कुठल्याही नैसर्गिक आपत्तीला किंवा इतर प्रकारचे नुकसान झाल्यास अनिल ट्रान्सपोर्ट जबाबदार राहणार नाही.आपल्या मालाचा इन्शुरन्स मालकाने करून घ्यावा </td>
               <td style="border:1px solid #ddd;">स्वाक्षरी</td>
               <td style="border:1px solid #ddd;" colspan="2"></td>
            </tr>
         </table>
      </main>
      <footer></footer>
   </body>
</html>