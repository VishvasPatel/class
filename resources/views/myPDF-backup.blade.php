<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Example 3</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <style>
      body {
      position: relative;
      width: 21cm;  
      height: 25cm; 
      margin: 0 auto; 
      color: #001028;
      background: #FFFFFF; 
      border: 1px solid #333;
      }
      table tr td {padding:2px; }
    table tr {
  page-break-inside: avoid;
}
 table tr td, table tr th {
        page-break-inside: avoid;
    }
   </style>
   <body>

      <main>
         <table style="font-family: arial;padding:5px 0px; width: 100%; font-size: 12px; ">
            <tr>
               <td style="border:1px solid #ddd;" colspan="4" rowspan="4">
                  <img src="{{asset('assets/img/logo2.png')}}" width="150px"><br>
                  <h4 style="margin: 5px 0px;">|| Shree. ||</h4>
                  <h2 style="margin: 5px 0px;">Anil Transport</h2>
                  <p style="margin: 5px 0px;">Transport Contractor & Commission Agent 1254, <br>Bhawani Peth, Pune 411042<br>Pune TO Sasvad, Jejuri, Morgaon,Walha Delhi service </p>
               </td>
               <td style="border:1px solid #ddd;font-weight:700;" width="25%">
                  Office Number
               </td>
               <td style="border:1px solid #ddd;" width="40%">26387674</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;font-weight:700;" width="25%">
                  Phone . 
               </td>
               <td style="border:1px solid #ddd; " width="40%"> 9970705454 / 8530855454</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;font-weight:700;" width="25%">
                  Lorry Number
               </td>
               <td style="border:1px solid #ddd;" width="40%">{{$invoice->lorry_number}}</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;font-weight:700;" width="25%">
                  Date
               </td>
               <td style="border:1px solid #ddd;" width="40%">{{ \Carbon\Carbon::now()->format('Y-m-d') }}                                          </td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">Receipt Number</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700"  >Sender Name</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">Mobile</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700 " width="30%">Receiver Name</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">City - </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">Court</td>
            </tr>
            <tr>
               <td>{{$invoice->receipt_number}}</td>
               <td>{{$invoice->material_sender}}  </td>
               <td>{{$invoice->mobile_number}}</td>
               <td>{{$invoice->material_receiver}}</td>
               <td>{{$invoice->city}}</td>
               <td>Pune Court</td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700" width="30%">Product</td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700" width="20%">
                  Brand
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">
                  Parcel Weight
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700 ">
                  Number of Parcel
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">
                  Parcel Charge
               </td>
               <td style="border:1px solid #ddd; background-color: #f6f0f0; font-weight:700">
                  Lorry Charges
               </td>
            </tr>
            
            @foreach($products as $product)
            <tr>
               <td style="border:1px solid #ddd; ">{{$product->product->name}}</td>
               <td style="border:1px solid #ddd; ">{{$product->brand->name}}</td>
               <td style="border:1px solid #ddd; ">{{$product->parcel_weight}}</td>
               <td style="border:1px solid #ddd;  ">{{$product->number_of_parcel}}</td>
               <td style="border:1px solid #ddd; ">{{$product->parcel_charge}}</td>
               <td style="border:1px solid #ddd; ">{{$product->parcel_charge * $product->number_of_parcel}}</td>
            </tr>
            @endforeach
            <tr>
               <td style="border:1px solid #ddd; "></td>
               <td style="border:1px solid #ddd; "></td>
               <td style="border:1px solid #ddd;font-weight:700;">
                  Total Number Parcel
               </td>
               <td style="border:1px solid #ddd;  ">
                  {{$invoice->total_number_of_parcel}}
               </td>
               <td style="border:1px solid #ddd;font-weight:700;">
                  Toal Lorry Charge
               </td>
               <td style="border:1px solid #ddd; ">
                   {{$invoice->lorry_charge}}
               </td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;font-weight:700;">Office Charge</td>
               <td style="border:1px solid #ddd; ">
                  {{$invoice->office_charge}}
               </td>
               <td style="border:1px solid #ddd;font-weight:700;">
                  Service Charge
               </td>
               <td style="border:1px solid #ddd;  ">
                  {{$invoice->service_charge}}
               </td>
               <td style="border:1px solid #ddd;font-weight:700;">
                  Total Charge
               </td>
               <td style="border:1px solid #ddd; ">
                   {{$invoice->total_charge}}
               </td>
            </tr>
            <tr>
               <td style="border:1px solid #ddd;font-weight:700;" colspan="3">  Invoice was created on a computer and is valid without the signature and seal.</td>
               <td style="border:1px solid #ddd;font-weight:700;">Signature</td>
               <td style="border:1px solid #ddd;" colspan="2"></td>
            </tr>
         </table>
      </main>
      <footer></footer>
   </body>
</html>