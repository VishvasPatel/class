@extends('layouts.app')

@section('content')
<div class="pagetitle">
      <h1>Create Product</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Create Product</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
 <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              
     <br> <br>
              <!-- General Form Elements -->
              <form method="post" action="{{route('submit-product')}}">
                  @csrf
                   <div class="row">
                       <div class="col-lg-6">
                           
                          
                           
                <div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">Product Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" required>
                  </div>
                </div>

<div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">Brand Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="brand_name">
                  </div>
                </div>
                          
               
                         
              
               
                       </div>
                       
                  </div>
                
              
               

                <div class="row mb-3">
               
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>

         
             
      </div>
    </section>
@endsection



  <!-- ======= Footer ======= -->
