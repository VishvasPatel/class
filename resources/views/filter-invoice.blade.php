@extends('layouts.app')
@push('styles')
<style>
         div.dataTables_paginate {
         text-align: center !important;
         float: none !important;
         margin-top: 20px;
         margin-bottom: 20px;
         }
         .center {
         text-align: center;
         }
         div.dataTables_paginate a {
         color: black !important;
         padding: 8px 16px !important;
         text-decoration: none !important;
         transition: background-color .3s !important;
         border: 1px solid #ddd !important;
         margin: 0 4px !important;
         display: inline-block;
         box-shadow: none;
         }
         div.dataTables_paginate a.active {
         background-color: #4CAF50 !important;
         color: white !important;
         border: 1px solid #4CAF50 !important;
         }
         div.dataTables_paginate a:hover:not(.active) {background-color: #ddd;}
         div.dataTables_paginate .current{   
         background-color:#0d6efd !important;
         border-color: #0d6efd !important;
         color: #fff !important;
         }
         .dataTables_wrapper .dataTables_paginate .paginate_button.current, 
         .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
         color: #fff !important;
         }
         .tablecustomscroll::-webkit-scrollbar {
         width: 1em;
         }
         .tablecustomscroll::-webkit-scrollbar-track {
         box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
         border-radius: 6px;
         }
         .tablecustomscroll::-webkit-scrollbar-thumb {
         background-color: #0d6efd;
         outline: 1px solid #0d6efd;
         border-radius: 6px;
         }
         .tablecustomscroll #example{margin-bottom: 20px;}
         @media only screen and (max-width: 479px){
         div.dataTables_paginate a{margin:5px !important;}
         .mobp{padding:15px !important;}
         .mobp .card-body {
         padding: 0px !important;
         }
         }
      </style>
@endpush

@section('content')
<div class="pagetitle">
      <h1>Filter Invoice</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Filter Invoice</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif
 <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              
     <br> <br>
              <!-- General Form Elements -->
              <form method="post" action="{{ route('filter-data') }}">
                  @csrf
                   <div class="row">
                       <div class="col-lg-6">
                           
                <div class="row mb-3">
                  <label for="inputText" class="col-sm-4 col-form-label">मोटार क्रमांक</label>
                  <div class="col-sm-8">
                       <input type="text" class="form-control" name="lorry_number">
                  </div>
                </div>
                          
               
                <div class="row mb-3">
                  <label for="inputPassword" class="col-sm-4 col-form-label">माल पाठवणार</label>
                  <div class="col-sm-8">
                                        
                           
                                
             <input type="text" class="form-control" name="material_sender">
                               
                               
                          
                            
                  </div>
                </div>
                           
                            <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">माल घेणार</label>
                  <div class="col-sm-8">
                      
                           
                               
                                                  <input type="text" class="form-control" name="material_receiver">

                               
                               
                          
                          
                   </div>
                </div>
              
               
                       </div>
                       
                        <div class="col-lg-6">
                            
                            
                
                              <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">प्रारंभ तारीख</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control"  id="start_date"   name="start_date">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">शेवटची तारीखल


</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control"  id="end_date" name="end_date">
                  </div>
                </div>
                
                <div class="row mb-3">
    <label for="city" class="col-sm-4 col-form-label">गांव</label>
    <div class="col-sm-8">
        <select name="city" id="city" class="form-control">

            <option value="" selected hidden>Select</option>
            <option value="सासवड" >   सासवड</option>
            <option value="जेजुरी" >जेजुरी</option>
            <option value="मोरगाव" > मोरगाव </option>
            <option value="वालहा" > वालहा </option>
            <option value="ढालेवाडी"> ढालेवाडी </option>
            <option value="कोथाळे "> कोथाळे  </option>
            <option value="धालेवाडी"> धालेवाडी </option>
            <option value="कोलविहरे" > कोलविहरे </option>
            <option value="शिवरी" > शिवरी</option>
            <option value="भिवरी" > भिवरी </option>
            <option value="भिवडी" > भिवडी </option>
            <option value="वीर" > वीर</option>
            <option value="हिवरे" > हिवरे</option>
            <option value="बोपगाव" > बोपगाव</option>
            <option value="चांबळी" > चांबळी </option>
            <option value="झेंडेवाडी " > झेंडेवाडी  </option>
            <option value="काळेवाडी" > काळेवाडी </option>
            <option value="दिवे" > दिवे </option>
            <option value="खळद " > खळद  </option>
            <option value="नारायणपूर" > नारायणपूर  </option>
            <option value="नारायणपेठ" > नारायणपेठ  </option>
            <option value="पवारवाडी " > पवारवाडी  </option>
            <option value="वाघापूर" > वाघापूर </option>
            <option value="सिंगापूर " > सिंगापूर  </option>
            <option value="दौंडज" > दौंडज </option>


        </select>
    </div>
</div>
                <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-4 col-form-label">  पेमेंट


</label>
                  <div class="col-sm-8">
                    <select name="payment" id="payment" class="form-control">
                  
 <option value="paid" >Paid</option>
  <option value="topay">ToPay</option>
</select>
                  </div>
                </div>
                
                             
                
               
               

                       </div>
                  </div>
                
              
               

                

        

            </div>
          </div>

        </div>

        
                
                
                
                
                
            
               
                       
                  
                 
                     
                     </div>
                    
                  </div>
                           
                           
                        

            </div>
          </div>

        </div>
        
        
          
              
          
          
             <div class="row mb-3">
               
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
      </div>
      </form>
    </section>
@endsection

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
  $("#product").change(function()
{
	var product=$("#product").val();
	$.ajax({
                url: "{{route('get-product')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "product": product
                },
                success: function(responce)
		{
			jQuery('#brand').empty();
			var data = responce.data;
			
			
			$.each(data, function (index, value) {
                
                $('#brand').append('<option value="' + value.brand_id + '">' + value.brand_name + '</option>');
			});

		}, 
            });
});
});


</script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function () {
         $('#example').DataTable({
         pagingType: 'full_numbers',
         "bInfo" : false,
         "lengthChange": false, 
         "aoColumns": [
         { sWidth: '38%' },
         { sWidth: '14%' },
         { sWidth: '16%' },
         { sWidth: '12%' },
         { sWidth: '10%' },
         { sWidth: '10%' },
         ],
         "searching": false
         });
         });
         
      </script>
      <script>
          function disabledField(id){
             $("#product_id_"+ id).prop("disabled",false);
             $("#brand_id_"+ id).prop("disabled",false);
             $("#parcel_weight_"+ id).prop("disabled",false);
             $("#parcel_charge_"+ id).prop("disabled",false);
             $("#number_of_parcel_"+ id).prop("disabled",false);
             $("#editbtn_"+ id).css("display","none");
             $("#updatebtn_"+ id).css("display","block");
          }
          
          function updateField(id){
            var product_id =  $("#product_id_"+ id).val();
           var brand_id =  $("#brand_id_"+ id).val();
            var parcel_weight = $("#parcel_weight_"+ id).val();
            var parcel_charge = $("#parcel_charge_"+ id).val();
            var number_of_parcel =  $("#number_of_parcel_"+ id).val();
            
             $.ajax({
                url: "{{route('update-invoice')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id" : id,
                    "product_id": product_id,
                    "brand_id": brand_id,
                    "parcel_weight": parcel_weight,
                    "parcel_charge": parcel_charge,
                    "number_of_parcel": number_of_parcel,
                    
                },
                success: function (response) {
                    if (response.status === true) {
                        window.location.reload();

                    }

                }, error: function (error) {

                }
            });
           
            
          }
      </script>
      <script>
          function getBrand(id){
              var product = $("#product_id_"+id).val();
              
              $.ajax({
                url: "{{route('get-product')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "product": product
                },
                success: function(responce)
		{
			jQuery('#brand_id_'+id).empty();
			var data = responce.data;
			
			
			$.each(data, function (index, value) {
                
                $('#brand_id_'+id).append('<option value="' + value.brand_id + '">' + value.brand_name + '</option>');
			});

		}, 
            });
          }
      </script>
      <script>
         function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
      </script>
      <script>
          $("#office_charge").on('input',function(){
              var office = $("#office_charge").val();
               $.ajax({
                url: "{{route('get-office-charge')}}",
                cache: false,
                method: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "office": office
                },
                success: function(responce)
		{
			if (responce.status === true) {
                        $("#total_charge").val(responce.data);

                    }

		}, 
            });
              
          })
      </script>
@endpush



  <!-- ======= Footer ======= -->
